//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:manage_without/Models/Details_Tracking.dart';
//import 'package:manage_without/Models/Event.dart';
//import 'package:manage_without/Models/Race.dart';
//
//import 'package:toast/toast.dart';
//
//import '../Database.dart';
//
//class UndoPage extends StatefulWidget {
//  Race race;
//  Event_l event_l;
//  int bib;
//
//  UndoPage(this.race, this.event_l, this.bib);
//
//  @override
//  _UndoPageState createState() => _UndoPageState(race, event_l, bib);
//}
//
//class _UndoPageState extends State<UndoPage> {
//  Race race;
//  Event_l event_l;
//  int bib;
//  DetailsTracking detailsTracking_new;
//
//  _UndoPageState(this.race, this.event_l, this.bib);
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: PreferredSize(
//        child: Container(
//          height: 150,
//          child: Padding(
//            padding: const EdgeInsets.only(top: 16.0),
//            child: Padding(
//              padding: const EdgeInsets.only(top: 16.0),
//              child: SingleChildScrollView(
//                scrollDirection: Axis.vertical,
//                child: Stack(
//                  children: <Widget>[
//                    Column(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        Row(
//                          children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Container(
//                                width: 80,
//                                height: 50,
//                                decoration: BoxDecoration(
//                                    image: DecorationImage(
//                                        image:
//                                            AssetImage('Images/LaLigaLogo.png'),
//                                        fit: BoxFit.fill)),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: SingleChildScrollView(
//                                scrollDirection: Axis.vertical,
//                                child: Column(
//                                  mainAxisAlignment: MainAxisAlignment.center,
//                                  crossAxisAlignment: CrossAxisAlignment.center,
//                                  children: <Widget>[
//                                    Text(
//                                      event_l.name,
//                                      style: TextStyle(
//                                          color: Colors.white,
//                                          fontSize: 20,
//                                          fontWeight: FontWeight.w100),
//                                    ),
//                                    Padding(
//                                      padding: const EdgeInsets.only(top: 4.0),
//                                      child: Text(
//                                        "${race.name}",
//                                        style: TextStyle(
//                                            color: Colors.white54,
//                                            fontSize: 20,
//                                            fontWeight: FontWeight.w100),
//                                      ),
//                                    ),
////                                    Padding(
////                                      padding: const EdgeInsets.only(top: 4.0),
////                                      child: Text(
////                                        "Racer Name",
////                                        style: TextStyle(
////                                            color: Colors.white,
////                                            fontSize: 20,
////                                            fontWeight: FontWeight.w100),
////                                      ),
////                                    ),
//                                  ],
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                        Padding(
//                          padding: const EdgeInsets.only(top: 8.0),
//                          child: Container(
//                            width: MediaQuery.of(context).size.width,
//                            color: Colors.blue,
//                            height: 2,
//                          ),
//                        )
//                      ],
//                    ),
//                    Align(
//                      alignment: Alignment.centerRight,
//                      child: Padding(
//                        padding: const EdgeInsets.only(top: 24, right: 12.0),
//                        child: Container(
//                          width: 25,
//                          height: 25,
//                          decoration: BoxDecoration(
//                              image: DecorationImage(
//                                  image:
//                                      AssetImage('Images/greenProfile.png'))),
//                        ),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ),
//        preferredSize: Size.fromHeight(110),
//      ),
//      backgroundColor: Colors.black,
//      body: Container(),
//    );
//  }
//
//  Future<void> load_detailsTracking() async {
//    await DBProvider.db
//        .DetailsTracking_limit1(race)
//        .then((value) => detailsTracking_new = value);
//    List  list_timings=detailsTracking_new.timings['timings'];
//    list_timings.forEach((el) {
//
////      Map<String,dynamic>te=el;
//      if(el.containsKey("temp")&&el['bib']==bib){
////        te.remove("temp");
//
//        _showMyDialog(el['temp']['timeMs']);
//        return;
//      }
//
//    });
//
//
//  }
//
//  Future<void> _showMyDialog(int mili) async {
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return AlertDialog(
//          backgroundColor: Colors.black,
//          title: Text('Do You want Undo?',        style: TextStyle(
//              color: Colors.white,
//              fontSize: 20,
//              fontWeight: FontWeight.w100)),
//          content: SingleChildScrollView(
//            scrollDirection: Axis.vertical,
//            child: Column(
//              children: <Widget>[
//                Container(
//                  child: Row(children: <Widget>[Text(
//                    "Bib: ",
//                    style: TextStyle(
//                        color: Colors.white,
//                        fontSize: 20,
//                        fontWeight: FontWeight.w100),
//                  ),Text(
//                    bib.toString(),
//                    style: TextStyle(
//                        color: Colors.white,
//                        fontSize: 20,
//                        fontWeight: FontWeight.w100),
//                  )],),
//                ),
//                Container(child: Row(children: <Widget>[Text( "Time:", style: TextStyle(
//                    color: Colors.white,
//                    fontSize: 20,
//                    fontWeight: FontWeight.w100)),Text( DateTime.fromMillisecondsSinceEpoch(mili).toString(), style: TextStyle(
//                    color: Colors.white,
//                    fontSize: 20,
//                    fontWeight: FontWeight.w100))],),)
//              ],
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('Cancel'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//            FlatButton(
//              child: Text('Ok'),
//              onPressed: () async {
//              await  undo();
//              Navigator.pop(context);
//              Navigator.pop(context);
//
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }
//Future<void>undo(){
//  List  list_timings=detailsTracking_new.timings['timings'];
//
//  list_timings.forEach((el) async {
//
////      Map<String,dynamic>te=el;
//    if(el.containsKey("temp")&&el['bib']==bib){
//      el.remove("temp");
//      el.remove("is_start");
//      await    detailsTracking_new.timings.update('timings', (value) => list_timings);
//      await DBProvider.db
//          .update_detailstracking(detailsTracking_new)
//          .then((value)async {
//        Toast.show("Complete", context,
//            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
////        setState(() {});
//
////        return;
//      });
//
//
//    }
//
//  });
////  Navigator.pop(context);
//}
//  @override
//  void initState() {
//    load_detailsTracking();
//
//    super.initState();
//  }
//}

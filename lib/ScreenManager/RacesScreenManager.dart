import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manage_without/Models/Course.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Manager.dart';
import 'package:manage_without/ScreenManager/LiveResultsManager.dart';
import 'package:manage_without/User/LoginManager.dart';

import '../Appearance.dart';
import 'ControlPage.dart';
import '../Database.dart';

import '../Models/Race.dart';
import 'EventsScreenManager.dart';

class RacesScreenManager extends StatefulWidget {
  Event_l event_l;

  RacesScreenManager(this.event_l);

  @override
  _RacesScreenManagerState createState() => _RacesScreenManagerState(event_l);
}

class _RacesScreenManagerState extends State<RacesScreenManager> {
  Event_l event_l;

  _RacesScreenManagerState(this.event_l);

  ScrollController _scrollController;

  List<Manager> manager_list = new List();
  List<Course>list_course=new List();
  Manager manager =
  new Manager(id: 1, email: "eeeee", password: '123345456788');
  Future<void> get_course_all() async {
    await  DBProvider.db.Course_list_all(event_l).then((value) {
      if(value.length>0){
        list_course=value;
      }
      else{
        list_course=[];
      }

    });

  }
  get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          manager_list = value;
        });
      }
      else if (value.length==0) {
        setState(() {
          manager_list=[];
        });

      }
    });
  }

  @override
  void initState() {
    manager_list.add(manager);
    get_course_all();
    _scrollController = ScrollController();
//    get_manager();

    super.initState();
  }
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await DBProvider.db.deleteAllManager().then((value) {
                  manager_list=[];
                  Navigator.of(context).pop();
                });
                setState(() {

                });
              },
            ),
          ],
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "${event_l.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(2),
                      child: Container(
                        child: Text(
                          "Races",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          if(manager_list.length==0 ){
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        LoginManager(event_l, null))).then((value) {
                              get_manager();
                            });
                          }
                          else {
                            _showMyDialog();
                          }
                        },
                        child: Container(
                          width: 20,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: manager_list.length == 0
                                      ? AssetImage('Images/profile.png')
                                      : AssetImage('Images/greenProfile.png'),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),
                    Container(
                      child: manager_list.length != 0
                          ? InkWell(
                        onTap: () {


                        },
                        child: Stack(
                          alignment: Alignment.topRight,
                          children: <Widget>[
                            Padding(
                              padding:
                              const EdgeInsets.only(right: 0, top: 5),
                              child: Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'Images/redChecked.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ),

                          ],
                        ),
                      )
                          : Container(
                        width: 25,
                        height: 25,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                AssetImage('Images/greyChecked.png'),
                                fit: BoxFit.fill)),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => EventsScreenManager(true)),
                          (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          Expanded(
            child: races_ListView(),
          ),
          Container(
            color: Colors.black,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.blue,
                    ),
                    width: 280,
                    height: 40,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) =>
                                    LiveResultsManager(event_l,true)));
                      },
                      child: Center(
                        child: Text(
                          "Liveresultat",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Back",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
  Color col_from_argb=Color(0xff121214);
  FutureBuilder<List<Race>> races_ListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Race_list_from_event(event_l),
      builder: (BuildContext context, AsyncSnapshot<List<Race>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                snapshot.data
                    .sort((a, b) => a.startTimeMs.compareTo(b.startTimeMs));
                Race race = snapshot.data[index];
                Course course = list_course.firstWhere(
                        (element) => element.id == race.courseId,
                    orElse: () => null);

                return  Padding(
                    padding: EdgeInsets.only(
                        left: 3, right: 3, top: index == 0 ? 15 : 2),
                    child: Container(
                      decoration: BoxDecoration(
                          color: col_from_argb,
                          border: Border(
                              bottom: index != snapshot.data.length-1
                                  ? BorderSide(
                                  color: Colors.blueGrey[50],
                                  width: 0.3)
                                  : BorderSide(),
                              top: index == 0
                                  ? BorderSide(
                                  color: Colors.blueGrey[50],
                                  width: 0.3)
                                  : BorderSide())),
                      height: 75,
                      child: InkWell(
                        onTap: () async {

                          manager_list.length == 0
                              ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginManager(event_l, race)))
                              : Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ControlPage(event_l, race)));
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(left: 8.0),
                                    child: Container(
//                                          width: 275,
                                      child: Text(
                                        race.name,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
//                                      Padding(
//                                        padding:
//                                            const EdgeInsets.only(left: 8.0),
//                                        child: ,
//                                      ),
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(left: 8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Start ${race.startTimeMs.toString().substring(10,16)}, ",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: font_size_body,
                                              fontWeight: FontWeight.w400),
                                        ),
                                        course != null
                                            ? Padding(
                                          padding: EdgeInsets.only(
                                              left:
                                              index == 0 ? 0 : 2),
                                          child: Container(
                                            child: Text(
                                              '${removeDecimalZeroFormat(double.parse((course.distance / 1000).toStringAsFixed(1)))} Km',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: font_size_body,
                                                  fontWeight:
                                                  FontWeight
                                                      .w400),
                                            ),
                                          ),
                                        )
                                            : Container()
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 16),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                    ))
                ;
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
                size: 17,
                color: Colors.blue,
              ));
        } else if (snapshot.data.length == 0) {
          return Center(
              child: Text(
                "No Races Here",
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
              ));
        }

        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.green,
          ),
        );
      },
    );
  }


  String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:manage_without/Appearance.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Manager.dart';
import 'package:manage_without/Models/Race.dart';


import '../Database.dart';



import 'package:intl/intl.dart' as nl;

import 'RacesScreenManager.dart';

class EventsScreenManager extends StatefulWidget {
  bool try_load;

  EventsScreenManager(this.try_load);

  @override
  _EventsScreenManagerState createState() => _EventsScreenManagerState(try_load);
}

class _EventsScreenManagerState extends State<EventsScreenManager> {
  bool try_load;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController _scrollController;
  List<Race> races = new List();
  bool is_finish_loading = false;
  List<Race> races_next = new List();
  List<Manager> manager_list = new List();
  Manager manager =
  new Manager(id: 1, email: "eeeee", password: '123345456788');

//  DateTime time_d=DateTime.fromMicrosecondsSinceEpoch(1599411600000);

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await  DBProvider.db.deleteAllManager().then((value) {
                  manager_list=[];
                  Navigator.of(context).pop();
                });
                setState(() {

                });
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> get_race_next() async {
    await  DBProvider.db.Race_list_false(DateTime.now()).then((data) {
      print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${data}");
      setState(() {
        races_next = data;
      });
    });
  }

  Widget ourBottomSheet() {
    get_race_next();
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(25), topLeft: Radius.circular(25)),
        color: Colors.blueAccent[200],
      ),
      height: 325,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 40,
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: races_next.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.1,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "${races_next[index].name}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w100),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Column(
                                    children: List.generate(
                                        races_next[index]
                                            .jso['splitNames']
                                            .length,
                                            (index2) => Text(
                                          races_next[index]
                                              .jso['splitNames'][index2],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: font_size_body,
                                              fontWeight: FontWeight.w100),
                                        )),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "${races_next[index].startTimeMs}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontWeight: FontWeight.w100),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16.0, top: 16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 8.0),
              child: Text(
                "Other events : ",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: font_size_body,
                    fontWeight: FontWeight.w700),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getarrangments() async {
    try {
      await http
          .get(
          'http://54.77.120.67:8080/rest/organizers/laliga/arrangements?class=App')
          .timeout(Duration(seconds: 3))
          .then((data) async {
//      races = [];
        var dataServer = jsonDecode(data.body);
        List temp = dataServer['arrangements'];


        await DBProvider.db.database;
        if (temp != null && dataServer.length > 0) {

          await DBProvider.db.deleteAllEvent();

          await DBProvider.db.insertallEvent(temp).then((value) {
            go_to_page();
            return;

          });

//        setState(() {});
        } else if (dataServer.length == 0) {

          await DBProvider.db.deleteAllEvent().then((value) {
            go_to_page();

          });
        }
      });
    } catch (ex) {
      print('ex>>>>>>>>>> ${ex}');
      final snackBar = new SnackBar(
          action: SnackBarAction(
            label: 'Undo',
            onPressed: () {
              // Some code to undo the change.
            },
          ),
          content: Row(
            children: <Widget>[
              Text(' please check internet connection'),
              InkWell(
                child: Icon(Icons.refresh),
                onTap: () {
//              _scaffoldKey.currentState.removeCurrentSnackBar();
                  _scaffoldKey.currentState.removeCurrentSnackBar();
                  getarrangments();
                },
              )
            ],
          ));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future<void> go_to_page() {
    Timer.periodic(Duration(seconds: 1), (timer) async {

      bool is_finish = DBProvider.db.is_finish_load;
      print('timer>>>> ${timer.tick} ${ DBProvider.db.is_finish_load}');
      if (is_finish) {
        setState(() {
          is_finish_loading=true;
        });
//        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_)=>EventsScreen(false)), (route) => false);
        timer.cancel();
        return;
      }
    });
  }

  get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          manager_list = value;
        });
      }
      else if (value.length==0) {
        setState(() {
          manager_list=[];
        });

      }
    });
  }

  Future<void> load() async {
    await get_manager();

  }
  @override
  void initState() {
    _scrollController = ScrollController();
    manager_list.add(manager);
//    getRaces();
//    get_manager();
    if(try_load){
      getarrangments();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,

      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 40
                ),
                child: Container(
//                  width: 150,
                  child: Text(
                    "La Liga Tävling",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 23,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 150,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        InkWell(
                          child: Padding(
                            padding: EdgeInsets.only(top: 60),
                            child: Container(
                              width: 80,
                              height: 50,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                      AssetImage('Images/LaLigaLogo.png'),
                                      fit: BoxFit.fill)),
                            ),
                          ),
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(builder: (_) => EventsScreenManager(true)),
                                    (route) => false);
                          },
                        ),
                        Expanded(
                          child: Center(
                            child:   Padding(
                              padding: const EdgeInsets.only(top: 70),
                              child: Center(
                                child: Container(
                                  width: 200,
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Text(
                                      "Dagens evenemang",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only( right: 20,top: 0,bottom: 4),
                          child: Container(
                            child: manager_list.length != 0
                                ? InkWell(
                              onTap: () {


                              },
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0, top: 0),
                                    child: Container(
                                      width: 25,
                                      height: 25,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/redChecked.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),

                                ],
                              ),
                            )
                                : Container(
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'Images/greyChecked.png'),
                                      fit: BoxFit.fill)),
                            ),
                          ),
                        ),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.end,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.blue,
                        height: 2,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body:Column(
        children: <Widget>[
          ! try_load? Expanded(
            child:  eventsListView(),
          ):try_load&&is_finish_loading?Expanded(
            child:  eventsListView(),
          ):SpinKitThreeBounce(size: font_size_body,color: Colors.blue,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: 25,
                child: InkWell(
                  onTap: () {
                    _scaffoldKey.currentState
                        .showBottomSheet((context) => ourBottomSheet(),
                        backgroundColor: Colors.black)
                        .closed
                        .then((val) {
                      setState(() {});
                    });
                  },
                  child: Stack(
                    children: <Widget>[
                      Text(
                        "Andra evenemang",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.w100),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 24.0),
                          child: Icon(
                            Icons.keyboard_arrow_up,
                            color: Colors.blue,
                            size: 30,
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
    );
  }
  Color col_from_argb=Color(0xff121214);
  FutureBuilder<List<Event_l>> eventsListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Events_list(),
      builder: (BuildContext context, AsyncSnapshot<List<Event_l>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                Event_l item = snapshot.data[index];
                print(item.toString());
                return Padding(
                  padding: EdgeInsets.only(
                      left: 3, right: 3, top: index == 0 ? 15 : 2),
                  child: Container(
                    decoration: BoxDecoration(
                        color: col_from_argb,
                        border: Border(
                            bottom: index != snapshot.data.length - 1
                                ? BorderSide(
                                color: Colors.blueGrey[50], width: 0.3)
                                : BorderSide(),top:  index == 0
                            ? BorderSide(
                            color: Colors.blueGrey[50], width: 0.3)
                            : BorderSide())),
                    height: 65,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => RacesScreenManager(item))).then((value) => get_manager())
                            .then((value) {
//                            DBProvider.db.Manager_list().then((value2) {
//                              print("Navigator : $value2");
//                              if(value2!=null&&value2.length>0){
//                                setState(() {
//                                  manager_list = value2;
//                                });
//                              }
//                              if(value2==null){
//                                manager_list.clear();
//                              }
//                            });
                        });
                      },
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
//                                    width: 275,
                                      child:
                                      Text('${item.name}, ${item.location}',
                                          //item.name
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1),
                                    ),
                                  ),
//                                Padding(
//                                  padding: const EdgeInsets.only(
//                                      left: 8.0, top: 2.0),
//                                  child: Container(
//                                    width: 225,
//                                    child: Text(
//                                      "Location : ${item.location != null ? item.location : '....'}",
//                                      //item.location
//                                      style: TextStyle(
//                                          color: Colors.white,
//                                          fontSize: 18,
//                                          fontWeight: FontWeight.w100),
//                                    ),
//                                  ),
//                                )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
                size: 17,
                color: Colors.blue,
              ));
        } else {
          return Center(
              child: Text(
                "No Events Here",
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
              ));
        }
//        return Center(
//          child: CircularProgressIndicator(
//            backgroundColor: Colors.green,
//          ),
//        );
      },
    );
  }

  _EventsScreenManagerState(this.try_load);
}



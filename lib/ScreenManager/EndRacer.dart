//
//import 'package:flutter/material.dart';
//import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
//import 'package:manage_without/Models/Coursepoints.dart';
//import 'package:manage_without/Models/Details_Tracking.dart';
//import 'package:manage_without/Models/Event.dart';
//import 'package:manage_without/Square.dart';
//
//import 'package:toast/toast.dart';
//
//import '../Database.dart';
//import 'Unidentified.dart';
//import '../Models/Race.dart';
//import 'package:http/http.dart' as http;
//
//class EndRacer extends StatefulWidget {
//  Race race;
//  Event_l event_l;
//
//  Square square;
//
//  EndRacer(this.race, this.event_l, this.square);
//
//  @override
//  _EndRacerState createState() => _EndRacerState(race, event_l, square);
//}
//
//class _EndRacerState extends State<EndRacer> {
//  Race race;
//  Event_l event_l;
//  Square square;
//
//  _EndRacerState(
//    this.race,
//    this.event_l,
//    this.square,
//  );
//
//  @override
//  void dispose() {
//    super.dispose();
//  }
//
//  bool is_dnf = false;
//  DateTime _dateTime;
//
//  Future<void> _showMyDialog() async {
//    DateTime temp;
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return AlertDialog(
//          backgroundColor: Colors.black,
//          title: Text('Set Time'),
//          content: SingleChildScrollView(
//            scrollDirection: Axis.vertical,
//            child: TimePickerSpinner(
//              normalTextStyle: TextStyle(fontSize: 24, color: Colors.grey),
//              highlightedTextStyle:
//                  TextStyle(fontSize: 24, color: Colors.white),
//              isShowSeconds: true,
//              is24HourMode: false,
//              spacing: 10,
//              itemHeight: 60,
//              isForce2Digits: true,
//              onTimeChange: (time) {
//                setState(() {
//                  temp = time;
//                });
//              },
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('Cancel'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//            FlatButton(
//              child: Text('Ok'),
//              onPressed: () {
//                if (temp != null) {
//                  setState(() {
//                    _dateTime = temp;
//                  });
//                }
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: PreferredSize(
//        child: Container(
//          height: 150,
//          child: Padding(
//            padding: const EdgeInsets.only(top: 16.0),
//            child: Padding(
//              padding: const EdgeInsets.only(top: 16.0),
//              child: SingleChildScrollView(
//                scrollDirection: Axis.vertical,
//                child: Stack(
//                  children: <Widget>[
//                    Column(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        Row(
//                          children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Container(
//                                width: 80,
//                                height: 50,
//                                decoration: BoxDecoration(
//                                    image: DecorationImage(
//                                        image:
//                                            AssetImage('Images/LaLigaLogo.png'),
//                                        fit: BoxFit.fill)),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: SingleChildScrollView(
//                                scrollDirection: Axis.vertical,
//                                child: Column(
//                                  mainAxisAlignment: MainAxisAlignment.center,
//                                  crossAxisAlignment: CrossAxisAlignment.center,
//                                  children: <Widget>[
//                                    Text(
//                                      event_l.name,
//                                      style: TextStyle(
//                                          color: Colors.white,
//                                          fontSize: 20,
//                                          fontWeight: FontWeight.w100),
//                                    ),
//                                    Padding(
//                                      padding: const EdgeInsets.only(top: 4.0),
//                                      child: Text(
//                                        "${race.name}",
//                                        style: TextStyle(
//                                            color: Colors.white54,
//                                            fontSize: 20,
//                                            fontWeight: FontWeight.w100),
//                                      ),
//                                    ),
////                                    Padding(
////                                      padding: const EdgeInsets.only(top: 4.0),
////                                      child: Text(
////                                        "Racer Name",
////                                        style: TextStyle(
////                                            color: Colors.white,
////                                            fontSize: 20,
////                                            fontWeight: FontWeight.w100),
////                                      ),
////                                    ),
//                                  ],
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                        Padding(
//                          padding: const EdgeInsets.only(top: 8.0),
//                          child: Container(
//                            width: MediaQuery.of(context).size.width,
//                            color: Colors.blue,
//                            height: 2,
//                          ),
//                        )
//                      ],
//                    ),
//                    Align(
//                      alignment: Alignment.centerRight,
//                      child: Padding(
//                        padding: const EdgeInsets.only(top: 24, right: 12.0),
//                        child: Container(
//                          width: 25,
//                          height: 25,
//                          decoration: BoxDecoration(
//                              image: DecorationImage(
//                                  image:
//                                      AssetImage('Images/greenProfile.png'))),
//                        ),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ),
//        preferredSize: Size.fromHeight(110),
//      ),
//      backgroundColor: Colors.black,
//      body: Stack(
//        children: <Widget>[
//          SingleChildScrollView(
//            scrollDirection: Axis.vertical,
//            child: Column(
//              children: <Widget>[
//                Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Container(
//                    width: MediaQuery.of(context).size.width,
//                    height: 260,
//                    color: Color(0xff121214),
//                    child: SingleChildScrollView(
//                      child: Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: <Widget>[
//                            Text(
//                              "Bib: ${square.lst_timings.last.bib}",
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontSize: 17,
//                                  fontWeight: FontWeight.w100),
//                            ),
//                            Column(
//                              mainAxisAlignment: MainAxisAlignment.start,
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: List.generate(
//                                  square.course.coursepoints.length,
//                                  (i) => square.course.coursepoints[i].label !=
//                                          square.course.coursepoints.last.label
//                                      ? Text(
//                                          "${square.course.coursepoints[i].label}: ${square.lst_timings.firstWhere((element) => element.courspoint == square.course.coursepoints[i].reference, orElse: () => null) != null ? square.lst_timings.firstWhere((element) => element.courspoint == square.course.coursepoints[i].reference).time : 'No Set'}",
//                                          style: TextStyle(
//                                              color: Colors.white,
//                                              fontSize: 17,
//                                              fontWeight: FontWeight.w100))
//                                      : InkWell(
//                                          onTap: () {
//                                            if (square.lst_timings.firstWhere(
//                                                    (element) =>
//                                                        element.courspoint ==
//                                                        square
//                                                            .course
//                                                            .coursepoints[i]
//                                                            .reference,
//                                                    orElse: () => null) ==
//                                                null) {
//                                              _showMyDialog();
//                                            }
//                                          },
//                                          child: Text(
//                                              "${square.course.coursepoints[i].label}: ${square.lst_timings.firstWhere((element) => element.courspoint == square.course.coursepoints[i].reference, orElse: () => null) != null ? square.lst_timings.firstWhere((element) => element.courspoint == square.course.coursepoints[i].reference).time : _dateTime == null ? 'Press ' : _dateTime}",
//                                              style: TextStyle(
//                                                  color: Colors.white,
//                                                  fontSize: 17,
//                                                  fontWeight: FontWeight.w100)),
//                                        )),
//                            ),
////                            Text("Starting time: 2:00:00",
////                                style: TextStyle(
////                                    color: Colors.white,
////                                    fontSize: 17,
////                                    fontWeight: FontWeight.w100)),
////                            Row(
////                              children: <Widget>[
////                                Text("Finish time:",
////                                    style: TextStyle(
////                                        color: Colors.white,
////                                        fontSize: 17,
////                                        fontWeight: FontWeight.w100)),
////                                InkWell(
////                                  onTap: () {
////                                    _showMyDialog();
////                                  },
////                                  child: _dateTime == null
////                                      ? Text(" Add",
////                                          style: TextStyle(
////                                              color: Colors.white,
////                                              fontSize: 17,
////                                              fontWeight: FontWeight.w300))
////                                      : Text(
////                                          " ${_dateTime.toString().substring(10, 19)}",
////                                          style: TextStyle(
////                                              color: Colors.white,
////                                              fontSize: 17,
////                                              fontWeight: FontWeight.w300)),
////                                ),
////                              ],
////                            ),
////                            Padding(
////                              padding: const EdgeInsets.only(
////                                  top: 16.0, bottom: 16.0),
////                              child: Column(
////                                children: List.generate(
////                                    race.jso['splitNames'].length, (index) {
////                                  String lap = race.jso['splitNames'][index];
////                                  return lap.contains("km")
////                                      ? Text("Man. Lap time ${lap}",
////                                          style: TextStyle(
////                                              color: Colors.white,
////                                              fontSize: 17,
////                                              fontWeight: FontWeight.w100))
////                                      : Container();
////                                }),
////                              ),
////                            ),
//                            Row(
//                              children: <Widget>[
//                                Text("The racer has end the race (DNF)",
//                                    style: TextStyle(
//                                        color: Colors.white,
//                                        fontSize: 17,
//                                        fontWeight: FontWeight.w100)),
//                                Padding(
//                                  padding: const EdgeInsets.only(left: 8.0),
//                                  child: Theme(
//                                    data: Theme.of(context).copyWith(
//                                      unselectedWidgetColor: Colors.white,
//                                    ),
//                                    child: Checkbox(
//                                      onChanged: (val) {
//                                        setState(() {
//                                          is_dnf = val;
//                                        });
//                                      },
//                                      value: is_dnf,
//                                    ),
//                                  ),
//                                )
//                              ],
//                            ),
//                          ],
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
//                Padding(
//                  padding: const EdgeInsets.only(top: 24.0),
//                  child: InkWell(
//                    onTap: () {
//                      if (square.lst_timings.firstWhere(
//                              (element) =>
//                                  element.courspoint ==
//                                  square.course.coursepoints.last.reference,
//                              orElse: () => null) ==
//                          null) {
//                        finish_racer(square.course.coursepoints.last);
//                      }
//
//                      print(square.detailsTracking.timings['timings']);
//                    },
//                    child: Container(
//                      decoration: BoxDecoration(
//                          color: Colors.blue,
//                          borderRadius: BorderRadius.all(Radius.circular(10))),
//                      width: 250,
//                      height: 50,
//                      child: Center(
//                        child: Text(
//                          "Confirm",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 15,
//                              fontWeight: FontWeight.w300),
//                        ),
//                      ),
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
//            child: Align(
//              alignment: Alignment.bottomLeft,
//              child: InkWell(
//                onTap: () => Navigator.pop(context),
//                child: Text(
//                  "Back",
//                  style: TextStyle(
//                      color: Colors.blue,
//                      fontSize: 15,
//                      fontWeight: FontWeight.w300),
//                ),
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//
//  Future<void> finish_racer(Coursepoints coursepoints) async {
//    Map<String, dynamic> bodys = {
//      'temp': {
//        "timeMs": _dateTime.millisecondsSinceEpoch,
//        "bib": square.lst_timings.last.bib,
//        "courspoint": coursepoints.reference,
//        "source": "manual",
//        "participantRef": square.lst_timings.last.participantRef,
//      }
//    };
//    if (bodys != null) {
//      DetailsTracking detailsTracking_new;
//      await DBProvider.db
//          .DetailsTracking_limit1(race)
//          .then((value) => detailsTracking_new = value);
////    List<Course> lst =
////    await DBProvider.db.Course_list_from_id(detailsTracking_new.id);
////    Course course = lst[0];
//
////      List lst_timings = detailsTracking_new.timings['timings'];
////      print("<<<<<<<dddd<<<< ${square.detailsTracking.timings}");
//      List list_timings = detailsTracking_new.timings['timings'];
//      list_timings.forEach((el) {
//        Map<String, dynamic> te = el;
//        print("SERARCH>>>> ${el}");
//        print('>>>> ${coursepoints.reference}');
//        if (
//            el['bib'] == square.bib) {
//          te.addAll(bodys);
//          te.addAll({'is_start':true});
//          el = te;
//          print(">>>>>>>EL ${el}");
//          print(">>>>>>>TE ${te}");
//        }
//      });
//
////      temp
//      print("before<>>>   ${square.detailsTracking.timings['timings']}");
//      await detailsTracking_new.timings
//          .update('timings', (value) => list_timings);
////        square.detailsTracking.timings = temp;
//
//      await DBProvider.db
//          .update_detailstracking(detailsTracking_new)
//          .then((value) async {
//        Toast.show("Complete", context,
//            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//        setState(() {});
//        Navigator.pop(context);
//      });
//    }
//
//
//
////        startRace(race,false);
//  }
//}

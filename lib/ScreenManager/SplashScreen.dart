import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../Database.dart';
import 'EventsScreenManager.dart';


class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer timer;
  var _scaffoldKey_splash = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey_splash,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0.0,
      ),
      backgroundColor: Colors.black,
      body: Stack (
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 30.0),
              child: Container(
                width: 300,
                height: 200,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('Images/LaLigaLogo.png'),
                        fit: BoxFit.fill)),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "XZPECT Studios",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w400),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getarrangments() async {
    try {
      await http
          .get(
          'http://54.77.120.67:8080/rest/organizers/laliga/arrangements?class=App')
          .timeout(Duration(seconds: 3))
          .then((data) async {
//      races = [];
        var dataServer = jsonDecode(data.body);
        List temp = dataServer['arrangements'];


        await DBProvider.db.database;
        if (temp != null && dataServer.length > 0) {

          await DBProvider.db.deleteAllEvent();

          await DBProvider.db.insertallEvent(temp).then((value) {
            go_to_page();
            return;

          });

//        setState(() {});
        } else if (dataServer.length == 0) {

          await DBProvider.db.deleteAllEvent().then((value) {
            go_to_page();

          });
        }
      });
    } catch (ex) {
      print('ex>>>>>>>>>> ${ex}');
      final snackBar = new SnackBar(
          action: SnackBarAction(
            label: 'Undo',
            onPressed: () {
              // Some code to undo the change.
            },
          ),
          content: Row(
            children: <Widget>[
              Text(' please check internet connection'),
              InkWell(
                child: Icon(Icons.refresh),
                onTap: () {
//              _scaffoldKey.currentState.removeCurrentSnackBar();
                  _scaffoldKey_splash.currentState.removeCurrentSnackBar();
                  getarrangments();
                },
              )
            ],
          ));
      _scaffoldKey_splash.currentState.showSnackBar(snackBar);
    }
  }

  Future<void> go_to_page() {
    Timer.periodic(Duration(seconds: 1), (timer) async {

      bool is_finish = DBProvider.db.is_finish_load;
      print('timer>>>> ${timer.tick} ${ DBProvider.db.is_finish_load}');
      if (is_finish) {

        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_)=>EventsScreenManager(false)), (route) => false);
        timer.cancel();
        return;
      }
    });
  }

  @override
  void initState() {
    getarrangments();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:manage_without/Models/Coursepoints.dart';
import 'package:manage_without/Models/Details_Tracking.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Manager.dart';
import 'package:manage_without/Models/Timings.dart';
import 'package:manage_without/Models/TrackingWithInformation.dart';
import 'package:manage_without/User/LoginManager.dart';

import 'package:toast/toast.dart';
import 'package:manage_without/Appearance.dart';

import '../Database.dart';
import '../Square.dart';
import 'EventsScreenManager.dart';
import 'Unidentified.dart';
import '../Models/Race.dart';
import 'package:http/http.dart' as http;

class ShowInformationRacer extends StatefulWidget {
  Race race;
  Event_l event_l;

  Square square;

  ShowInformationRacer(this.race, this.event_l, this.square);

  @override
  _ShowInformationRacerState createState() =>
      _ShowInformationRacerState(race, event_l, square);
}

class _ShowInformationRacerState extends State<ShowInformationRacer> {
  Race race;
  Event_l event_l;
  Square square;
  DetailsTracking detailsTracking;
  bool is_finish_load = false;

  bool send_to_server=false;

  _ShowInformationRacerState(
    this.race,
    this.event_l,
    this.square,
  );

  bool is_dnf = false;
  bool is_dns = false;
  bool is_dsq = false;


  List<Manager> manager_list = new List();
  List<Timing> list_timings = new List();

  @override
  void dispose() {
    super.dispose();
  }

//  Future<void> _showMyDialog() async {
//
//    DateTime temp;
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return AlertDialog(
//          backgroundColor: Colors.black,
//          title: Text('Set Time'),
//          content: SingleChildScrollView(
//            scrollDirection: Axis.vertical,
//            child: TimePickerSpinner(
//              normalTextStyle: TextStyle(fontSize: 24, color: Colors.grey),
//              highlightedTextStyle:
//                  TextStyle(fontSize: 24, color: Colors.white),
//              isShowSeconds: true,
//              is24HourMode: false,
//              spacing: 10,
//              itemHeight: 60,
//              isForce2Digits: true,
//              onTimeChange: (time) {
//                setState(() {
//                  temp = time;
//                });
//              },
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('Cancel'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//            FlatButton(
//              child: Text('Ok'),
//              onPressed: () {
//                if (temp != null) {
//                  setState(() {
//                    _dateTime = temp;
//                  });
//                }
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "${event_l.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      child: Text(
                        "${race.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),  Container(
                      child: Text(
                        "${square.bib}.${race.list_racers.firstWhere((element) => element.bib==square.bib,orElse: ()=>null)!=null?race.list_racers.firstWhere((element) => element.bib==square.bib,orElse: ()=>null).firstname+' '+race.list_racers.firstWhere((element) => element.bib==square.bib,orElse: ()=>null).surname:''}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          if(manager_list.length==0 ){
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                                        LoginManager(event_l, null))).then((value) {
                              get_manager();
                            });
                          }
                        },
                        child: Container(
                          width: 20,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: manager_list.length == 0
                                      ? AssetImage('Images/profile.png')
                                      : AssetImage('Images/greenProfile.png'),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => EventsScreenManager(true)),
                          (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: is_finish_load
          ? Stack(
              children: <Widget>[
                SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding:
                          const EdgeInsets.only( left: 8.0, right: 8.0, top:  15 ),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color(0xff121214),
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.blueGrey[50],
                                        width: 0.3))),
                            width: MediaQuery.of(context).size.width,
                            child: SingleChildScrollView(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Bib: ${square.bib}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: font_size_body,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: square.course.coursepoints
                                          .map((e) => Text(
                                              "${e.label}: ${list_timings.firstWhere((element) => element.courspoint == e.reference, orElse: () => null) != null ? list_timings.firstWhere((element) => element.courspoint == e.reference).time : 'No Set'}",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: font_size_body,
                                                  fontWeight: FontWeight.w400)))
                                          .toList(),
                                    ),

                                    get_checkbox()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 24.0,left: 20,right: 20),
                          child: !send_to_server?InkWell(
                            onTap: () async {
                  if(is_dnf||is_dns||is_dsq){

                 await   change_status_racer(extract_value());

                  }
                  else{
                    Navigator.pop(context);
                  }

                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),

                              height: 50,
                              child: Center(
                                child: Text(
                                  "Confirm",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ):SpinKitThreeBounce(color: Colors.blue,size: font_size_body,),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: () => Navigator.pop(context),
                      child: Text(
                        "Back",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                )
              ],
            )
          : Container(),
    );
  }

  String extract_value(){
    if(is_dnf){
      return "dnf";
    }
    else if(is_dns){
      return "dns";
    }
    else if(is_dsq){
      return "dsq";
    }
  }
  Future<void> change_status_racer(String note) async {
    setState(() {
      send_to_server=true;
    });
    TrackingInformation trackingInformation;
  await  DBProvider.db.TrackingInformation_list_from_race(race).then((value) {

    trackingInformation=value[0];
    });
    var bodys = jsonEncode({
      "status" : note
    });
  try{
    print('>>>>>>>>>>>>>> ${trackingInformation.reference}');
    await http.post(
        'http://54.77.120.67:8080/rest/_App/trackings/${trackingInformation.reference}/trackers/${square.bib}/result',
        body: bodys,
        headers: <String, String>{
          'Content-Type': 'application/json;charset=UTF-8'
        }).then((data) async {
//      races = [];
      Map<String, dynamic> dataServer = jsonDecode(data.body);
//print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ${dataServer}");
      if (dataServer.isNotEmpty) {
        print('>>>>>>>>>dataServer>>>>> ${dataServer}');
        setState(() {
          send_to_server=false;
        });
        Toast.show("succeeded!!!!", context);
        Navigator.pop(context);

      }else{
        Toast.show("an error occurred ):", context);
        setState(() {
          send_to_server=false;
        });
      }
//print

    });
  }
  catch(ex){
    setState(() {
      send_to_server=false;
    });
  }
  }
  get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          manager_list = value;
        });
      }
      else if (value.length==0) {
        setState(() {
          manager_list=[];
        });

      }
    });
  }
  Widget get_checkbox() {
    return Column(
      children: <Widget>[
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("The racer has start the race (DNS)",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: font_size_body,
                    fontWeight: FontWeight.w400)),
            alig_checbox(Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: Colors.white,
              ),
              child: Checkbox(
                onChanged: (val) {
                  make_checkbox_fasle();
                  setState(() {
                    is_dns = val;
                  });
                },
                value: is_dns,
              ),
            ))
          ],
        ),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween ,
          children: <Widget>[
            Text("The racer has q the race (DSQ)",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: font_size_body,
                    fontWeight: FontWeight.w400)),
           alig_checbox( Theme(
             data: Theme.of(context).copyWith(
               unselectedWidgetColor: Colors.white,
             ),
             child: Checkbox(
               onChanged: (val) {
                 make_checkbox_fasle();
                 setState(() {
                   is_dsq = val;
                 });
               },
               value: is_dsq,
             ),
           ))
          ],
        ),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween ,
          children: <Widget>[
            Text("The racer has end the race (DNF)",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: font_size_body,
                    fontWeight: FontWeight.w400)),
         alig_checbox(Theme(
           data: Theme.of(context).copyWith(
             unselectedWidgetColor: Colors.white,
           ),
           child: Checkbox(
             onChanged: (val) {
               make_checkbox_fasle();
               setState(() {
                 is_dnf = val;
               });
             },
             value: is_dnf,
           ),
         ))
          ],
        )
      ],
    );
  }

  Future<void> get_detail() async {
    await DBProvider.db.DetailsTracking_limit1(race).then((value) {
      if (value != null) {
        setState(() {
          detailsTracking = value;
          is_finish_load = true;
          list_timings = detailsTracking.lst_timings
              .where((element) => element.bib == square.bib)
              .toList();
        });
      }
    });
  }

  void make_checkbox_fasle() {
    is_dsq = false;
    is_dnf = false;
    is_dns = false;
  }

  Widget alig_checbox(Widget widget) {
    return  Align(
      alignment: Alignment.bottomRight,
      child: widget,
    );
  }

  @override
  void initState() {
    get_detail();
    super.initState();
  }
}

import 'package:flutter/material.dart';
import 'package:manage_without/Appearance.dart';
class EndRace extends StatefulWidget {
  @override
  _EndRaceState createState() => _EndRaceState();
}

class _EndRaceState extends State<EndRace> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            color: Color(0xff121214),
            height: 90,
            child: Row(
              children: <Widget>[
                Icon(Icons.keyboard_arrow_down , color: Colors.blue,),
                Text("End Rac" ,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: font_size_body,
                      fontWeight: FontWeight.w400), )
              ],
            ),
          )
        ],
      ),
    );
  }
}

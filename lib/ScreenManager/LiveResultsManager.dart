import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manage_without/Models/Course.dart';
import 'package:manage_without/Models/Coursepoints.dart';
import 'package:manage_without/Models/Details_Tracking.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Racer.dart';
import 'package:manage_without/Models/Timings.dart';
import 'package:manage_without/Models/TrackingWithInformation.dart';
import 'package:manage_without/ColorsF.dart' as col;

import '../Appearance.dart';
import '../Database.dart';
import '../Models/Race.dart';
import 'EventsScreenManager.dart';

class LiveResultsManager extends StatefulWidget {
  Event_l event_l;
bool with_appar;

  LiveResultsManager(this.event_l, this.with_appar);



  @override
  _LiveResultsManagerState createState() => _LiveResultsManagerState(event_l,with_appar);
}

class _LiveResultsManagerState extends State<LiveResultsManager> {
  bool with_appar;

  _LiveResultsManagerState( this.event_l,this.with_appar,);

  Event_l event_l;



  DetailsTracking detailsTracking;
  bool is_finish_from_server = false;
  Course course;
  List<Race> list_races = new List();
  List<TrackingInformation> lst_tracking = new List();
  bool is_complite_race = false;
  bool is_complite_course = false;
  Race race_selected;
  Coursepoints coursepoints;
  List<Timing> finish_timings = new List();
  List<Course>list_course=new List();
  var _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,

      appBar:with_appar? PreferredSize(
          child:Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(top: 13),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "${event_l.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2),
                        child: Container(
                          child: Text(
                            "Liveresultat",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              Container(
                child: InkWell(
                  child: Padding(
                    padding: EdgeInsets.only(top: 60),
                    child: Container(
                      width: 80,
                      height: 50,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('Images/LaLigaLogo.png'),
                              fit: BoxFit.fill)),
                    ),
                  ),
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => EventsScreenManager(true)),
                            (route) => false);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 120),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.blue,
                  height: 2,
                ),
              ),
            ],
          ),
          preferredSize: Size.fromHeight(100),
        ):null,
        backgroundColor: Colors.black,
        body:  SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                const EdgeInsets.only(top: 15.0, left: 8.0, right: 8.0),
                child: is_complite_race
                    ? Container(
                  decoration: BoxDecoration(
                      color: col.col_from_argb,
                      border: Border(


                          top: BorderSide(
                              color: Colors.blueGrey[50],
                              width: 0.3)
                      )),
                  child: ExpansionTile(
                    title: Row(
                      children: <Widget>[
//                                  Container(
//                                      child: Icon(
//                                    Icons.keyboard_arrow_down,
//                                    color: Colors.blue,
//                                  )),
                        Text(
                            race_selected != null
                                ? race_selected.name
                                : 'please selected race',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400))
                      ],
                    ),
                    children: <Widget>[
                      Container(
                        width:
                        MediaQuery.of(context).size.width / 1.1,
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.all(
                                Radius.circular(15))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                children: List.generate(
                                    list_races.length, (index) {
                                  return Padding(
                                    padding:
                                    const EdgeInsets.only(top: 4),
                                    child: InkWell(
                                      onTap: () async {
                                        setState(() {
                                          race_selected =
                                          list_races[index];
                                          is_complite_course = false;
                                        });
                                        await get_information_from_server(
                                            race_selected);
                                      },
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                            height: 35,
                                            child: ListTile(
                                              leading: Text(
                                                "${list_races[index].name}${list_course.firstWhere((element) => element.id==list_races[index].courseId,orElse: ()=>null)!=null?', ${removeDecimalZeroFormat(double.parse((list_course.firstWhere((element) => element.id==list_races[index].courseId,orElse: ()=>null).distance/1000).toStringAsFixed(1)))} Km':''} ",
                                                style: TextStyle(
                                                    color:
                                                    Colors.white,
                                                    fontSize: font_size_body,
                                                    fontWeight:
                                                    FontWeight
                                                        .w400),
                                              ),
//                                                  trailing: Checkbox(
//                                                    onChanged: (val) async {
//                                                      lapsList[index].select =
//                                                          val;
//                                                      if (lapsList[0].select ==
//                                                          true) {
//                                                        await fill_search_list(
//                                                            list_search_temp
//                                                                .toList());
//                                                      } else {
//                                                        await fill_search_list(
//                                                            lapsList
//                                                                .where((element) =>
//                                                            element
//                                                                .select ==
//                                                                true)
//                                                                .toList());
//                                                      }
//                                                      load_square_list();
//                                                    },
//                                                    value:
//                                                    lapsList[index].select,
//                                                  ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
                    : SpinKitThreeBounce(
                  size: font_size_body,
                  color: Colors.blue,
                ),
              ),
              is_complite_course && course != null
                  ? Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: Container(
                  color: col.col_from_argb,
                  child: ExpansionTile(
                    title: Row(
                      children: <Widget>[
//                            Container(
//                                child: Icon(
//                                  Icons.keyboard_arrow_down,
//                                  color: Colors.blue,
//                                )),
                        coursepoints != null&&(coursepoints.reference==course.coursepoints.last
                            .reference||coursepoints.reference==course.coursepoints.first
                            .reference)?
                        Text(
                            coursepoints.reference==course.coursepoints.first
                                .reference
                                ? '${course.coursepoints.first
                                .label}'
                                : 'Målgång ${removeDecimalZeroFormat(double.parse((course.coursepoints.last.distance/1000).toStringAsFixed(1)))} km',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400))
                            :
                        coursepoints == null?  Text(
                            'please selected coursepoint',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w100)):Text(
                            '${coursepoints.label} ${removeDecimalZeroFormat(double.parse((course.coursepoints.last.distance/1000).toStringAsFixed(1)))} km',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400))
                      ],
                    ),
                    children: <Widget>[
                      Container(
                        width:
                        MediaQuery.of(context).size.width / 1.1,
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.all(
                                Radius.circular(15))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                  children: course.coursepoints.map((e) => Padding(
                                    padding:
                                    const EdgeInsets.only(top: 4),
                                    child: Container(
                                      child: InkWell(
                                        onTap: () async {
                                          setState(() {
                                            coursepoints = e;
                                          });
                                          setState(() {

                                            is_complite_course = false;
                                          });
                                          await get_information_from_server(
                                              race_selected,reset: false);
//                                              ckeck();
//                                                  refresh_result();
                                        },
                                        child: Stack(
                                          children: <Widget>[
                                            Container(
                                              height: 35,
                                              child: ListTile(
                                                leading:(e.reference==course.coursepoints.last
                                                    .reference||e.reference==course.coursepoints.first
                                                    .reference)? Text(
                                                  "${e.reference==course.coursepoints.first.reference? e.label:'Målgång ${removeDecimalZeroFormat(double.parse((e.distance/1000).toStringAsFixed(1)))} km'} ",
                                                  style: TextStyle(
                                                      color: Colors
                                                          .white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                      FontWeight
                                                          .w400),
                                                ):Text(
                                                  "${removeDecimalZeroFormat(double.parse((e.distance.toInt() / 1000).toStringAsFixed(1)))} km",
                                                  style: TextStyle(
                                                      color: Colors
                                                          .white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                      FontWeight
                                                          .w400),
                                                ),
//
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )) .toList()
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
                  : Container(),
              is_complite_course && detailsTracking != null
                  ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: col.col_from_argb,
                      border: Border(


                          top: BorderSide(
                              color: Colors.blueGrey[50],
                              width: 0.3)
                      )),
                  child: Column(
                      children: finish_timings
                          .where((element) => coursepoints != null
                          ? element.courspoint ==
                          coursepoints.reference
                          : element.bib != null)
                          .map((e) {
                        var timing = e;
                        Racer racer;
                        if (race_selected != null &&
                            race_selected.list_racers.firstWhere(
                                    (element) =>
                                element.bib == timing.bib,
                                orElse: () => null) !=
                                null) {
                          racer = race_selected.list_racers.firstWhere(
                                  (element) => element.bib == timing.bib);
                        }
                        String cl;
                        if (racer != null && racer.club != null) {
                          print('>>>>>CL ${racer.club}');
                          cl = racer.club;
                        }
                        List<Timing> list = new List();
                        list.addAll(finish_timings.where((element) =>
                        coursepoints != null
                            ? element.courspoint ==
                            coursepoints.reference
                            : element.bib != null));

                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(

                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: <Widget>[
                                SingleChildScrollView(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: get_arrow(e),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Text(
                                              '${list.indexWhere((element) => element == e) + 1} . ${timing.bib}. ${racer != null ? racer.firstname.substring(0, 1).toUpperCase() : 'unknow'}.${racer != null ? racer.surname.toUpperCase() : ''}',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: font_size_body,
                                                  fontWeight:
                                                  FontWeight.w400)),
//                                        width: 150,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            right: 60),
                                        child: Text(
                                            '${DateTime.fromMillisecondsSinceEpoch(timing.duration).hour}:${DateTime.fromMillisecondsSinceEpoch(timing.duration).minute}:${DateTime.fromMillisecondsSinceEpoch(timing.duration).second}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight:
                                                FontWeight.w400)),
                                      ),
                                    ],
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                  ),
                                  scrollDirection: Axis.vertical,
                                ),
                                Text(' ${cl != null ? cl : ''}',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontWeight: FontWeight.w400)),
                              ],
                            ),
                          ),
                        );
                      }).toList()),
                ),
              )
                  : Container()
            ],
          ),
        ));
  }

  Future<void> get_information_from_server(Race race,{bool reset=true}) async {
    try {
      setState(() {
        if(reset){
          course = null;
          coursepoints = null;
        }
        finish_timings = [];
        detailsTracking = null;
        is_complite_race = false;
      });
      await DBProvider.db.get_information_race(race,event_l).then((value) async {
        await refresh_informa(race);
      });
    } catch (ex) {
      print('ex>>>>>>>>>>>>>> ${ex}');
      setState(() {
        is_complite_race = true;
        is_finish_from_server = true;
      });
    }
  }

  Coursepoints get_coursepoint(String label, Course course) =>
      course.coursepoints.firstWhere(
              (element) => element.reference.toUpperCase() == label.toUpperCase());

  Future<void> refresh_informa(Race race) async {
    await DBProvider.db
        .TrackingInformation_list_from_race(race)
        .then((value) async {
      lst_tracking = value;

      await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
//        race = value[0];
        race_selected = value[0];
      });
    });

    await DBProvider.db
        .DetailsTracking_list_from_race(race)
        .then((value) async {
      if (value.length > 0) {
        detailsTracking = value[0];
        print(">>>>>>>>>>>>>>>>>>>>>> ${detailsTracking.lst_timings.length}");
        await get_course(race, state: false);
        print("beforesort>>>> ${detailsTracking.lst_timings}");

        refresh_result();
      }
    });
    await get_course(race);
    setState(() {
      is_complite_race = true;
      is_finish_from_server = true;
      print('>>>>>time_of_race ${race_selected.startTimeMs.millisecondsSinceEpoch}');
    });

// await  get_races();
  }

  void ckeck() {
    finish_timings.forEach((element) {
      print('e.>>>>>>>>>>> ${element}');
    });
  }

  Widget get_arrow(Timing timing) {

    List<Timing> all_timings = detailsTracking.lst_timings
        .where((element) => element.bib == timing.bib)
        .toList();
    all_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
        .compareTo(get_coursepoint(b.courspoint, course)));
    DateTime time_tim = DateTime.fromMillisecondsSinceEpoch(timing.duration);
    if (all_timings.length == 1) {
      return Icon(
        Icons.arrow_forward,

        color: Colors.yellow,
      );
    }
    else if(all_timings.length==2){

      var last_timing = all_timings[all_timings.indexOf(timing) - 1];
      DateTime time_last = DateTime.fromMillisecondsSinceEpoch(last_timing.duration);
      DateTime time_new =time_tim;
      DateTime time_of_race = DateTime(time_last.year,time_last.month,time_last.day,race_selected.startTimeMs.hour,race_selected.startTimeMs.minute,race_selected.startTimeMs.second,race_selected.startTimeMs.millisecond);

      print('>>>>>time_of_race ${time_of_race}');
      print('>>>>>time_new ${time_new}');
      print('>>>>>time_last ${time_last}');

      if (time_last.difference(time_of_race)==time_new.difference(time_last)) {

        return Icon(
          Icons.arrow_forward,

          color: Colors.yellow,
        );
      } else if (time_last.difference(time_of_race)<time_new.difference(time_last)) {
        return Icon(
          Icons.arrow_downward,
          color: Colors.red,
        );
      } else if (time_last.difference(time_of_race)>time_new.difference(time_last)) {
        return Icon(
          Icons.arrow_upward,

          color: Colors.green,
        );
      }

    }
    else if(all_timings.length>2){
      var last_timing = all_timings[all_timings.indexOf(timing) - 1];
      var last_last_timing = all_timings[all_timings.indexOf(timing) - 2];
      DateTime time_last = DateTime.fromMillisecondsSinceEpoch(last_timing.duration);

      DateTime time_last_last =DateTime.fromMillisecondsSinceEpoch(last_last_timing.duration);
      DateTime time_new =time_tim;



      if (time_last.difference(time_new)==time_last_last.difference(time_last)) {

        return Icon(
          Icons.arrow_forward,

          color: Colors.yellow,
        );
      } else if (time_new.difference(time_last)>time_last.difference(time_last_last)) {
        return Icon(
          Icons.arrow_downward,
          color: Colors.red,
        );
      } else if (time_new.difference(time_last)<time_last.difference(time_last_last)) {
        return Icon(
          Icons.arrow_upward,

          color: Colors.green,
        );
      }

    }


  }

  void refresh_result() {
    detailsTracking.lst_timings.sort((a, b) =>
        get_coursepoint(a.courspoint, course)
            .compareTo(get_coursepoint(b.courspoint, course)));
    print("aftersort>>>> ${detailsTracking.lst_timings}");
    finish_timings = [];
    List<int> list_bib = new List();
    print('>>>>>>>>>>>>>>>>>list_bib ${list_bib}');
    detailsTracking.lst_timings.forEach((element) {
      if (!list_bib.contains(element.bib)) {
        list_bib.add(element.bib);
      }
    });
    print('>>>>>>>>>>>>>>>>>list_bib ${list_bib}');
    list_bib.forEach((eli) {
      finish_timings.add(detailsTracking.lst_timings
          .lastWhere((element) => element.bib == eli));
    });
    //        finish_timings.sort((a, b) => b.duration.compareTo(a.duration));
    print("aftersort>>>> ${detailsTracking.lst_timings}");
    print("finish_timings>>>>>>>>>>>>>>>>> ${finish_timings.length}");
    print("finish_refrseh>>>>>>>>>>>>>>>>>");
  }

  Future<void> get_course(Race race, {bool state = true}) async {
    await DBProvider.db.Course_from_race(race).then((value) {
      setState(() {
        course = value;
        is_finish_from_server = state;
        is_complite_course = state;
      });
    });
  }

  get_races() async {
    await DBProvider.db.Race_list_from_event(event_l).then((value) {
      setState(() {
        value.sort((a, b) => a.id.compareTo(b.id));
        list_races = value;
        is_complite_race = true;
      });
    });
    get_course_all();
  }
  Future<void> get_course_all() async {
    await  DBProvider.db.Course_list_all(event_l).then((value) {
      if(value.length>0){
        list_course=value;
      }
      else{
        list_course=[];
      }

    });

  }
  @override
  void initState() {

    get_races();

//    get_information_from_server();
    super.initState();
  }
}
String removeDecimalZeroFormat(double n) {
  return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
}


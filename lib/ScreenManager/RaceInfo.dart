import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Racer.dart';
import 'package:manage_without/User/AddRacer.dart';
import 'package:manage_without/User/UpdateRacer.dart';

import '../Database.dart';
import '../Models/Course.dart';

import '../Models/Race.dart';
import 'EventsScreenManager.dart';
import 'package:manage_without/Appearance.dart';

class RaceInfo extends StatefulWidget {
  Event_l event_l;
  Race race;

  RaceInfo(this.event_l, this.race);

  @override
  _RaceInfoState createState() => _RaceInfoState(event_l, race);
}

class _RaceInfoState extends State<RaceInfo> {
  Event_l event_l;
  Race race;

  _RaceInfoState(this.event_l, this.race);

//  List<Racer> raceList = new List();
  Course course = new Course();
  bool doneRaces = false;
  bool donecourses = false;

  //  get_laps() {
//    DBProvider.db.LapsTimes_list(loop.id).then((data) {
//      setState(() {
//        lapsList = data;
//        print(data);
//      });
//    });
//  }
//  Future<void> get_information_race() async {
//    try {
//      await http
//          .get(
//              'http://54.77.120.67:8080/rest/races/${race.reference}/startlist?class=App')
//          .then((data) async {
////      races = [];
//        var dataServer = jsonDecode(data.body);
//
//        List racers = dataServer['race']['startlist'];
//
//        if (dataServer.length > 0) {
//          DBProvider.db.insertallRacer(racers, race);
//
//          setState(() {});
//        } else if (dataServer.length == 0) {
//          DBProvider.db.delete_from_race(race);
//          setState(() {});
//        }
//      });
//    } catch (ex) {
//      get_racers();
//    }
//
//    get_racers();
//  }

  @override
  void dispose() {

    super.dispose();
  } //  void getRacers() async {
//    print(loop.id);
//    try {
//      await http.post('$url/view_racer_in_loop.php', body: {
//        'Id_loop': loop.id.toString(),
//      }).then((data) async {
//        List dataServer = await jsonDecode(data.body);
//        print(dataServer);
//        raceList.clear();
//        dataServer.forEach((element) {
//          Racer racer = Racer.fromJSONserver(element);
//          print(racer.toString());
//          setState(() {
//            raceList.add(racer);
//          });
//        });
//        setState(() {
//          doneRaces = true;
//        });
//        print("/////////////////////////////");
//        print(raceList);
//      });
//    } catch (ex) {
////      Toast.show("Try Again !", context,
////          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//
//      print(ex);
//    }
//  }

//  get_racers() {
//    DBProvider.db.Racer_list_from_race(race).then((value) {
//      setState(() {
//        raceList.clear();
//        raceList = value;
//        doneRaces = true;
//        get_course_race(race);
//      });
//    });
//  }

//  Future<void> get_course_race(Race race) async {
//    try {
//      await http
//          .get(
//              'http://54.77.120.67:8080/rest/courses/${race.courseId}?class=App')
//          .then((data) async {
////      races = [];
//        Map<String, dynamic> dataServer = jsonDecode(data.body);
//
//        Map<String, dynamic> course = dataServer;
//        print(">>>>>>>>>>>>>>>>>>>>>>> database1");
//        if (dataServer != null && course.containsKey('course')) {
//          course = dataServer['course'];
//          await DBProvider.db.insertallcourses(course, race);
//
//          get_course();
//        } else if (dataServer.length == 0) {
////          await DBProvider.db.delete_from_course(race);
//          donecourses = true;
////
////          get_course();
//        }
//      });
//    } catch (ex) {
//      get_course();
//    }
//  }

  get_course() {
    DBProvider.db.Course_from_race(race).then((value) {
      setState(() {
        course = value ;
        donecourses = true;
        print(value.toString());
      });
    });
  }

  @override
  void initState() {
    DBProvider.db.get_information_race(race,event_l).then((value) {
      refresh_race();
    });

    super.initState();
  }

  Future<void> refresh_race() async {
  setState(() {
    doneRaces = false;
  });
    await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
      if (value.length > 0) {
        setState(() {
          race = value[0];


        });
      }
      setState(() {
        doneRaces = true;
      });
    });
  get_course();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 30, left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "${event_l.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),

                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 10, left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Info ${race.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),

                  ],
                ),
              ),
            ),

            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => EventsScreenManager(true)),
                          (route) => false);
                },
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only( left: 8.0, right: 8.0, top:  15 ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 350,
              decoration: BoxDecoration(
                  color: Color(0xff121214),
                  border: Border(

                      top:  BorderSide(
                          color: Colors.blueGrey[50],
                          width: 0.3)
                          )),

              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 3.0),
                        child: Text(
                          "Starting time: ${race.startTimeMs.toString().substring(10, 16)}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: donecourses && doneRaces
                              ? Text(
                                  "Distance: ${course != null ? course.distance != null ?removeDecimalZeroFormat(double.parse((course.distance / 1000).toStringAsFixed(1)))+' km'  : '...' : '...' }",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                )
                              : SpinKitThreeBounce(
                                  size: 17,
                                  color: Colors.blue,
                                )),
                      Padding(
                        padding: const EdgeInsets.only(left: 0.0, bottom: 3.0),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Mellantider: ",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: font_size_body,
                                    fontWeight: FontWeight.w400),
                              ),
                              course != null && donecourses
                                  ? Row(
                                      children: <Widget>[
                                        ...course.coursepoints.where((element) => element.reference!=course.coursepoints.last.reference&&element.reference!=course.coursepoints.first.reference).map((e) => Padding(
                                          padding:
                                          const EdgeInsets.only(left: 4),
                                          child: Text(
                                            '${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        )).toList()
                                      ],
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Checka in-racers:",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      doneRaces
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, bottom: 3.0),
                              child: race.list_racers.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 16.0),
                                      child: Center(
                                        child: Text(
                                          "No Racers !!",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: font_size_body,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: List.generate( race.list_racers.length,
                                            (index) {
                                          return  race.list_racers[index].firstname !=
                                                      null &&
                                              race.list_racers[index].surname !=
                                                      null
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${ race.list_racers[index].firstname.toString()} ${ race.list_racers[index].surname.toString()} ",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: font_size_body,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                )
                                              : Container();
                                        }),
                                      ),
                                    ),
                            )
                          :  race.list_racers.length != 0
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: Center(
                                    child: Text(
                                      "No Racers !!",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: font_size_body,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                )
                              : Padding(
                                  padding: EdgeInsets.only(top: 24.0),
                                  child: SpinKitThreeBounce(
                                    size: font_size_body,
                                    color: Colors.blue,
                                  ),
                                )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Text(
                  "Back",
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: font_size_body,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }
}

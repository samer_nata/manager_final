import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';


import 'Models/Course.dart';
import 'Models/Details_Tracking.dart';
import 'Models/Event.dart';

import 'Models/Manager.dart';
import 'Models/Note.dart';
import 'Models/Race.dart';

import 'package:http/http.dart' as http;

import 'Models/TrackingWithInformation.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "race_manager_new_ap_ref.db");
    return await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {


      await db.execute(
          "CREATE TABLE Manager (id int PRIMARY KEY ,email TEXT ,password TEXT);");

      await db.execute(
          "CREATE TABLE Events (startTimeMs INTEGER ,defaultRace TEXT,endTimeMs INTEGER ,location TEXT,name TEXT UNIQUE ,reference TEXT);");
      await db.execute(
          "CREATE TABLE Races (id int PRIMARY KEY,startTimeMs INTEGER ,name TEXT,jso JSON,racers JSON,courseId int,reference TEXT,is_start BOOLEAN,ref_event TEXT,FOREIGN KEY (ref_event) REFERENCES Events(reference)  on DELETE CASCADE on update CASCADE);");

      await db.execute(
          "CREATE TABLE Tracking (id int PRIMARY KEY,duration INTEGER ,startTimeMs INTEGER,courseId int,status TEXT,openingTimeMs INTEGER,gunTimeMs INTEGER,race TEXT,reference TEXT UNIQUE,ref_event TEXT,FOREIGN KEY (reference) REFERENCES Races(reference)  on DELETE CASCADE on update CASCADE ,FOREIGN KEY (ref_event) REFERENCES Events(reference)  on DELETE CASCADE on update CASCADE );");
      await db.execute(
          "CREATE TABLE Details_Tracking (timings JSON, trackers JSON,reference TEXT ,reference_race TEXT,id_p int PRIMARY KEY,id int ,FOREIGN KEY (id) REFERENCES Tracking(id)  on DELETE CASCADE on update CASCADE );");

      await db.execute(
          "CREATE TABLE Course (id int  PRIMARY KEY,distance REAL,name TEXT,startPos JSON,endPos JSON,js_coursepoints JSON);");

      await db.execute("PRAGMA foreign_keys = ON;");

    });
  }

  open() async {
    if (_database != null) {
      await openDatabase("race_manager_new_ap_ref.db");
    }
  }

  Future<void> close() async {
//    await refresh_state_app();
    await _database.close();
  }

/**********************************************************************************/
  bool is_finish_load = false;

  /*****************************END NOTE******************************************************************/
  /************************************Event*******************************************/

  Future<void> insert_event(Event_l event) async {
    await _database.insert(
      'Events',
      event.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertallEvent(List listrace) async {
    is_finish_load = false;
    final db1 = await database;
    listrace.forEach((obj) async {
      Event_l event = new Event_l.fromJSONServer(obj);
      print('event>>>>>>>>>>> ${event.toString()}');
      try {
        db1
            .insert('Events', event.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace)
            .then((value) async {
          if (value != null && obj['races'] != null) {
            print("pbj>>>>> ${obj}");
            List temp2 = obj['races'];
            await insertallRace(temp2, event);
          }
        });
      } catch (ex) {
        print("ex<<<<<<<<  ${ex}");
      }
      print("obj>>>>>>>>>> ${obj}");
      if (listrace.indexOf(obj) == listrace.length - 1) {


      }
    });
    return;
  }

  Future<List<Event_l>> Events_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Events');
    return List.generate(maps.length, (i) {
      return Event_l.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllEvent() async {
    final db = await database;


    await db.delete("Events");
    return;
  }

  /******************************End Event******************************************************/
  /*************************************Ra_RAC**********************************************/


  /***********************************End_ra_race*********************************************************/
  /**********************Race*****************************************************************************************/

  Future<void> insert_race(Race race) async {
    await _database.insert(
      'Races',
      race.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_race(Race race) async {
    await _database.update(
      'Races',
      race.toMap(),
      where: "id = ? ",
      whereArgs: [race.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
//    print(race.toString());
  }

//  Future<void> update_race(Race race, int id) async {

//    await _database.update(
//      'Races',
//      race.toMap(),
//      where: "id = ?",
//      whereArgs: [id],
//      conflictAlgorithm: ConflictAlgorithm.replace,
//    );
//  }

  Future<void> get_information_race(Race race,Event_l event_l) async {
    await http
        .get(
            'http://54.77.120.67:8080/rest/_App/races/${race.reference}/startlist/participants')
        .then((data) async {
//      races = [];
      Map<String, dynamic> dataServer = jsonDecode(data.body);
//      LisSt racers = dataServer['race']['startlist'];
      print("dataServer>>>>> ${dataServer}");
      race.racers = dataServer['race'];
      await insert_race(race).then((value) async {
        await get_course_race(race);
        await get_trackings_race(race, event_l,is_update: false);
      });

//      if (dataServer.length > 0) {
//        DBProvider.db.insertallRacer(racers, race);
//      } else if (dataServer.length == 0) {
//        DBProvider.db.delete_from_race(race);
//      }
    });
  }

  Future<void> get_course_race(Race race) async {
    await http
        .get('http://54.77.120.67:8080/rest/_App/courses/${race.courseId}')
        .then((data) async {
//      races = [];
      Map<String, dynamic> dataServer = jsonDecode(data.body);

      if (dataServer != null && dataServer.containsKey('course')) {
        await DBProvider.db.insertallcourses(dataServer['course'], race);
      } else if (dataServer.length == 0) {
        await DBProvider.db.delete_from_course(race);
      }
    });
  }

  Future<void> get_trackings_race(Race race,Event_l event_l, {bool is_update,}) async {
    try {
      await http
          .get('http://54.77.120.67:8080/rest/_App/trackings/${race.reference}')
          .timeout(Duration(seconds: 3))
          .then((data) async {
//      races = [];
        if (data.statusCode != 200) {
          throw HttpException('>>>>xx${data.statusCode}');
        }
        Map<String, dynamic> dataServer = jsonDecode(data.body);

        if (is_update) {
          TrackingInformation trackingInformation =
          new TrackingInformation.fromJSONSERVER(
              dataServer['tracking'], race,event_l);
          await update_tracking(trackingInformation);
          DetailsTracking detailsTracking = new DetailsTracking.fromJSONSERVER(
              dataServer['tracking'], trackingInformation, race);
          await DBProvider.db.insert_detailstracking(detailsTracking);

          return;
        }
        if (dataServer.length > 0) {
          TrackingInformation trackingInformation =
          new TrackingInformation.fromJSONSERVER(
              dataServer['tracking'], race,event_l);
          DetailsTracking detailsTracking = new DetailsTracking.fromJSONSERVER(
              dataServer['tracking'], trackingInformation, race);
//          await DBProvider.db.delete_DetailsTracking(trackingInformation);
          await DBProvider.db.insert_tracking(trackingInformation);

          await DBProvider.db.insert_detailstracking(detailsTracking);
        } else if (dataServer.length == 0) {
          await DBProvider.db.delete_TrackingInformation(race);
        }
        return;
      });
    } on TimeoutException catch (e) {
//      print('Timeout Error: $e');
    } on SocketException catch (e) {
//      print('Socket Error: $e');
    } on Error catch (e) {
//      print('General Error: $e');
    }

    print('endtracking>>>>>${race.name}>>>>>');
  }

  Future<void> insertallRace(List listrace, Event_l event_l) async {
//    final db1 = await database;

    listrace.forEach((obj) async {
      Race race = new Race.fromJSONSERVER(obj);
      race.ref_event = event_l.reference;
      print("race>>>>> ${race.toString()}");
      print('listrace>>>>>>>>>>>>> ${listrace}');
      try {
//        await db1.insert('Races', race.toMap(),
//            conflictAlgorithm: ConflictAlgorithm.replace);
        if (listrace.indexOf(obj) == listrace.length - 1) {
          await get_information_race(race,event_l);
          is_finish_load = true;
        } else {
          await get_information_race(race,event_l);
        }
      } catch (ex) {
        print("ex>>>>>>>>> ${ex}");
      }
    });

    return;
  }

  Future<List<Race>> Race_list_state_true() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Races',
    );
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_false(DateTime dateTime) async {
//    safsaf
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Races',
        where: " startTimeMs  > ? ",
        whereArgs: [dateTime.millisecondsSinceEpoch]);
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_from_event(Event_l event_l) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('Races', where: "ref_event = ?", whereArgs: [event_l.reference]);
    print('maps>>>>>>>>>>>>>>>> ${maps}');
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_all_e() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Races');

    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }
  Future<List<Race>> Race_list_refresh(Race race, Event_l event_l) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.rawQuery(
        "Select * from Races where ref_event='${race.ref_event}' and id=${race.id} ");

    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllRace() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete(
      "Races",
    );
    return;
  }

  Future<void> deleteAllRace_from_Event(Event_l event_l) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Races", where: "ref_event", whereArgs: [event_l.reference]);
    return;
  }

//

  /******************************************end race***************************************************/
  /******************************************COURSE*******************************************************************/

  Future<void> insert_course(Course course) async {
    await _database.insert(
      'Course',
      course.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertallcourses(Map<String, dynamic> js, Race race) async {
    final db1 = await database;
//    await delete_from_course(race);

    Course course = new Course.fromJSONServer(js);

    try {
      db1
          .insert('Course', course.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace)
          .then((value) {
//            await insert_Ra_RAC(ra_rac);
      });
    } catch (ex) {}

    return;
  }

  Future<List<Course>> Course_list(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Course',
        where: 'id = ?', whereArgs: [race.courseId], limit: 1);
    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<List<Course>> Course_list_from_id(int id) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM COURSE WHERE id = ( SELECT courseId from Tracking where id = ${id} )");

    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<List<Course>> Course_list_all(Event_l event_l) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * from Course where id in (SELECT courseId from Races where ref_event = '${event_l.reference}')");
    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<Course> Course_from_race(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Course',
      where: 'id = ?',
      whereArgs: [race.courseId],
    );

    return maps.length>0? Course.fromJSON(maps[0]):null;
  }

  Future<void> delete_from_course(Race race) async {
    final db = await database;

    await db.execute("DELETE from Course where id = ${race.id}");
    return;
  }

  /****************************************END COURSE******************************************************************************/
  /*****************************Racer **********************************************/












  /***************************End Racer************************************************/
  Future<void> insert_manager(Manager manager) async {
    await _database.insert(
      'Manager',
      manager.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Manager>> Manager_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM Manager");

    return maps != null
        ? List.generate(maps.length, (i) {
            return Manager.fromJSON(maps[i]);
          })
        : null;
  }

  Future<void> deleteAllManager() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Manager");
    return;
  }

  /**********************************end manager ****************************************************/

  /**************************************loops***************************************************************/

  /**************************************TRACKING_WITH_INFORMATIN***************************************************************************************/

  Future<void> insert_tracking(TrackingInformation trackingInformation) async {
    await _database.insert(
      'Tracking',
      trackingInformation.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_tracking(TrackingInformation trackingInformation) async {
    await _database.update(
      'Tracking',
      trackingInformation.toMap(),
      where: "reference = ?",
      whereArgs: [trackingInformation.reference],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertalltracking(Map<String, dynamic> js, Race race,Event_l event_l) async {
    final db1 = await database;
//    await delete_from_course(race);
    db1.transaction((db) async {
      TrackingInformation trackingInformation =
      new TrackingInformation.fromJSONSERVER(js, race,event_l);

      try {
        db
            .insert('Tracking', trackingInformation.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace)
            .then((value) {
//            await insert_Ra_RAC(ra_rac);
        });
      } catch (ex) {}
    });

    return;
  }

  Future<List<TrackingInformation>> TrackingInformation_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Tracking',
    );

    return List.generate(maps.length, (i) {
      return TrackingInformation.fromJSON(maps[i]);
    });
  }

  Future<List<TrackingInformation>> TrackingInformation_list_from_race(
      Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('Tracking', where: "reference = ?", whereArgs: [race.reference]);
    return List.generate(maps.length, (i) {
      return TrackingInformation.fromJSON(maps[i]);
    });
  }

  Future<void> delete_all_TrackingInformation() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from TrackingInformation ");
    return;
  }

  Future<void> delete_TrackingInformation(Race race) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute(
        "DELETE from TrackingInformation where reference = ${race.reference}");
    return;
  }

  /******************************END********  TRACKING_WITH_INFORMATIN******************************************************************************************************************/

  /**************************************DETAILS Tracking*******************************************************************************************************************/
  Future<void> insert_detailstracking(DetailsTracking detailsTracking) async {
    await _database.insert(
      'Details_Tracking',
      detailsTracking.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_detailstracking(DetailsTracking detailsTracking) async {
    await _database.update(
      'Details_Tracking',
      detailsTracking.toMap(),
      where: "id = ?",
      whereArgs: [detailsTracking.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }



  Future<List<DetailsTracking>> detailstracking_list_from_event(
      Event_l event_l) async {
    List<DetailsTracking> lst_details = new List();
    Race_list_from_event(event_l).then((lst_race) {
      lst_race.forEach((race) {
        DetailsTracking_list_from_race(race).then((value) {
          lst_details.addAll(value);
        });
      });
    });

    return lst_details;
  }

  Future<List<DetailsTracking>> DetailsTracking_list_from_race(
      Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Details_Tracking',
        where: "reference_race = ?", whereArgs: [race.reference]);

    return List.generate(maps.length, (i) {
      return DetailsTracking.fromJSON(maps[i]);
    });
  }

  Future<DetailsTracking> DetailsTracking_limit1(Race race) async {
    final db = await database;
//    afs.reference_race
    List<Map<String, dynamic>> maps = await db.query('Details_Tracking',
        where: " reference_race = ?", whereArgs: [race.reference], limit: 1);
    return DetailsTracking.fromJSON(maps[0]);
  }

  Future<List<DetailsTracking>> DetailsTracking_list(Event_l event_l) async {
    final db = await database;

    ///INNER JOIN  Races ON  Tracking.reference = Races.reference where ref_event = ${event_l.name} where Races.reference in (SELECT reference from Races where ref_event =${event_l.name})
    List<Map<String, dynamic>> maps_new = await db.rawQuery(
        "SELECT * from Details_Tracking  where reference_race in(SELECT reference from Races where ref_event = '${event_l.name}')  ORDER BY Details_Tracking.id DESC");
    return List.generate(maps_new.length, (i) {
      return DetailsTracking.fromJSON(maps_new[i]);
    });
  }

  Future<void> delete_all_DetailsTracking() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from Details_Tracking ");
    return;
  }

  Future<void> delete_DetailsTracking(Race race) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute(
        "DELETE from Details_Tracking where reference_race = ${race.name}");
    return;
  }
/******************************END***************************DETAILS Tracking*****************************************************************************************************************/
}

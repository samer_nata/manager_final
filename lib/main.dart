import 'dart:convert';

import 'package:flutter/material.dart';
import 'ScreenManager/ControlPage.dart';
import 'ScreenManager/EventsScreenManager.dart';


import 'Database.dart';

import 'ScreenManager/SplashScreen.dart';

void main() {
  runApp(MyApp());
  DBProvider.db.database.then((value) => DBProvider.db.open());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var key = GlobalKey();

  /*****samer****/

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: key,
      debugShowCheckedModeBanner: false,


      home: SplashScreen(),
    );
  }
}

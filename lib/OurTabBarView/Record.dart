import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:animated_widgets/widgets/opacity_animated.dart';
import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

import 'package:http/http.dart' as http;
import 'package:manage_without/Appearance.dart';
import 'package:manage_without/Models/Course.dart';
import 'package:manage_without/Models/Coursepoints.dart';
import 'package:manage_without/Models/Details_Tracking.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Race.dart';
import 'package:manage_without/Models/Timings.dart';
import 'package:manage_without/Models/TrackingWithInformation.dart';
import 'package:manage_without/ScreenManager/ShowInformationRacer.dart';
import 'package:manage_without/ScreenManager/UndoPage.dart';
import 'package:manage_without/ScreenManager/UnidentifiedWithNote.dart';
import 'package:manage_without/Square.dart';

import 'package:toast/toast.dart';

import '../Database.dart';
import '../ScreenManager/EndRacer.dart';
import '../ScreenManager/Unidentified.dart';

class Record extends StatefulWidget {
  Event_l event_l;

  Race race;

  Record(this.event_l, this.race);

  @override
  _RecordState createState() => _RecordState(event_l, race);
}

class _RecordState extends State<Record> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isAll = false;
  Race race;
  Timer _timer;
  Event_l event_l;
  bool is_finish = false;

  List<Coursepoints> list_search = new List();
  List<Coursepoints> list_search_temp = new List();

  List<Race> lst_race = new List();
  List<Coursepoints> lapsList = new List();
  List<Coursepoints> lapsList_for_undifind = new List();
  SpinKitThreeBounce spinKitThreeBounce = SpinKitThreeBounce(
    color: Colors.blue,
    size: 17,
  );
  bool cpmplite_load = false;

//  List<Course>list_course=new List();
  List<Square> list_square_orange = new List<Square>();
  List<Square> list_square_blue = new List<Square>();
  List<Square> list_square_grey = new List<Square>();
  List<Square> list_square_grey_io = new List<Square>();
  Course course;

//  List<Square> orang_list = new List<Square>();

  Coursepoints value_filter = new Coursepoints(lapIndex: -9, label: 'Show all');

//
//  List grey_list = ["O|"];
//  List grey_list_io = ["O| 1"];

//  List grey_list = new List();
//  List grey_list_io = new List();

//  List orang_list_r = ["76F", "101F", "102F", "67F", "68F"];
//  List blue_list_r = ["201", "202", "203"];
//  List grey_list_r = ["O|"];
  Future<void> fill_search_list(List<Coursepoints> list) {
    print("list>>>>>>>>>>>>>>> ${list}");
//    list_search=[];
//    list_search.add()
    setState(() {
      list_search = list;
    });
    list_search.insert(0, new Coursepoints(lapIndex: -9, label: 'Show all'));
  }

  Future<void> load_square_list() async {
    list_square_orange = [];
    list_square_blue = [];

    list_square_grey = [];

//    List<DetailsTracking> lst_detal = new List();
    DetailsTracking detailsTracking_new;
    await DBProvider.db
        .DetailsTracking_limit1(race)
        .then((value) => detailsTracking_new = value);

//    lst_detal.sort((a, b) => a.id.compareTo(b.id));
//    print("afttttt ${detafloilsTracking_new}");
    List<Course> list_course = new List();
    await add_square_first_load(detailsTracking_new, list_course);
//    print("DetailsTracking_list.length ${DetailsTracking_list.length}");

    list_square_grey.add(new Square(txt: "OI"));
    //  list_square_grey_io.add(square);
    setState(() {});
  }

  Future add_square_first_load(
      DetailsTracking detal, List<Course> list_course) async {
    list_course = await DBProvider.db.Course_list_from_id(detal.id);
    Course course = list_course[0];

    List<Timing> lst_timings = detal.lst_timings.toList(growable: true);

    lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
        .compareTo(get_coursepoint(b.courspoint, course)));
    List<int> list_bib = new List();
    lst_timings.forEach((element) {
      if (!list_bib.contains(element.bib)) {
        list_bib.add(element.bib);
      }
    });

    list_bib.forEach((element) {
      Square square = new Square(
          course: course,
          function: () {
            setState(() {});
          },
          detailsTracking: detal,
          bib: element,
          lst_timings: lst_timings
              .where((el) => el.bib == element)
              .toList(growable: true),
          is_start_from_page: false,
          time_in_mill:
              lst_timings.lastWhere((el) => el.bib == element).temp_time != null
                  ? lst_timings.lastWhere((el) => el.bib == element).temp_time
                  : null,
          timerMaxSeconds: 12);
      square.lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
          .compareTo(get_coursepoint(b.courspoint, course)));

      if (square.lst_timings.last.courspoint ==
              course.coursepoints.last.reference &&
          list_search.firstWhere(
                  (element) =>
                      element.reference ==
                      get_coursepoint(
                              square.lst_timings.last.courspoint, course)
                          .reference,
                  orElse: () => null) !=
              null) {
        list_square_orange.add(square);
      } else if (square.lst_timings.last.courspoint !=
              course.coursepoints.last.reference &&
          list_search.firstWhere(
                  (element) =>
                      element.reference ==
                      get_coursepoint(
                              square.lst_timings.last.courspoint, course)
                          .reference,
                  orElse: () => null) !=
              null) {
        square.km = list_search
            .firstWhere(
                (element) =>
                    element.reference ==
                    get_coursepoint(square.lst_timings.last.courspoint, course)
                        .reference,
                orElse: () => null)
            .distance;
        list_square_blue.add(square);
        //

      }
    });
  }

  Future<void> load_square_list_refresh() async {
//    list_square_orange.clear();

    DetailsTracking detailsTracking_new;
    await DBProvider.db
        .DetailsTracking_limit1(race)
        .then((value) => detailsTracking_new = value);

//    lst_detal.sort((a, b) => a.id.compareTo(b.id));    lst_detal.sort((a, b) => a.id.compareTo(b.id));
//    print("detailsTracking_new<<<<<> ${detailsTracking_new}");
    if (value_filter.lapIndex != -9) {
      list_square_blue = [];
    }
    await add_square_second_load(detailsTracking_new);
//    print("DetailsTracking_list.length ${DetailsTracking_list.length}");
    list_square_grey = [];
//    list_square_grey_io = [];
    list_square_grey.add(new Square(txt: "OI"));

    setState(() {});
    return;
  }

  Future<void> add_square_second_load(DetailsTracking detailsTracking) async {
    List<Course> lst =
        await DBProvider.db.Course_list_from_id(detailsTracking.id);
    Course course = lst[0];

    List<Timing> lst_timings =
        detailsTracking.lst_timings.toList(growable: true);

    lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
        .compareTo(get_coursepoint(b.courspoint, course)));
    List<int> list_bib = new List();
    lst_timings.forEach((element) {
      if (!list_bib.contains(element.bib)) {
        list_bib.add(element.bib);
      }
    });

    list_bib.forEach((element) {
      Square square = new Square(
          course: course,
          function: () {
            print('>>>>>>>>>>>>>function');
          },
          detailsTracking: detailsTracking,
          bib: element,
          lst_timings: lst_timings
              .where((el) => el.bib == element)
              .toList(growable: true),
          is_start_from_page: false,
          time_in_mill:
              lst_timings.lastWhere((el) => el.bib == element).temp_time != null
                  ? lst_timings.lastWhere((el) => el.bib == element).temp_time
                  : null,
          timerMaxSeconds: 12);
      square.lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
          .compareTo(get_coursepoint(b.courspoint, course)));

      if (square.lst_timings.last.courspoint ==
              course.coursepoints.last.reference &&
          list_search.firstWhere(
                  (element) =>
                      element.reference ==
                      get_coursepoint(
                              square.lst_timings.last.courspoint, course)
                          .reference,
                  orElse: () => null) !=
              null &&
          list_square_orange.firstWhere((els) => els.bib == element,
                  orElse: () => null) ==
              null) {
        list_square_orange.add(square);
      } else if (square.lst_timings.last.courspoint !=
              course.coursepoints.last.reference &&
          list_search.firstWhere(
                  (element) =>
                      element.reference ==
                      get_coursepoint(
                              square.lst_timings.last.courspoint, course)
                          .reference,
                  orElse: () => null) !=
              null &&
          list_square_blue.firstWhere((els) => els.bib == element,
                  orElse: () => null) ==
              null) {
        square.km = list_search
            .firstWhere(
                (element) =>
                    element.reference ==
                    get_coursepoint(square.lst_timings.last.courspoint, course)
                        .reference,
                orElse: () => null)
            .distance;
        list_square_blue.add(square);
        //

      }
    });
    return;
  }

//  Future<Square> get_square_timer(Timing temp, Course course, Square square,
//      Square square_temp, List<Square> lst) async {
//    lst.forEach((element) {
////      print("here>>>>${element}");
//    });
//
////    print("square ${square}");
////    print("square_temp ${square_temp}");
//
//    if (lst.lastWhere((el) => el.bib == square.bib, orElse: () => null) !=
//        null) {
////      print(
////          "bbbbb ${lst.lastWhere((el) => el.bib == square.bib, orElse: () => null)}");
//      square = lst.lastWhere((el) => el.bib == square.bib, orElse: () => null);
//
////      print("new squ>>> ${square}");
//
//      square.lst_timings = square_temp.lst_timings;
//      square.detailsTracking = square_temp.detailsTracking;
//      square.course = square_temp.course;
////      print("new squ>>> ${square}");
//      if (square != null) return square;
//    }
//    print(">>>>>>>>>>>>>>");
//
//    return null;
//  }

  Coursepoints get_coursepoint(String label, Course course) =>
      course.coursepoints.firstWhere(
          (element) => element.reference.toUpperCase() == label.toUpperCase());

  Future<void> stop_all() async {
    list_square_orange.forEach((element) async {
      if (element.is_start_from_page && !element.is_finish) {
        await element.cancel();
      }
    });
    list_square_blue.forEach((element) async {
      if (element.is_start_from_page && !element.is_finish) {
        await element.cancel();
      }
    });
  }

  Future start_all() async {
    list_square_orange.forEach((element) async {
//      print("elbeforestart>>> ${element.bib}");
      if (element.is_start_from_page && !element.is_start) {
//        print("elafterstart>>> ${element.bib}");

        await element.start();
      }
    });
    list_square_blue.forEach((element) async {
      if (element.is_start_from_page && !element.is_start) {
        await element.start();
      }
    });
//    setState(() {});
  }

  @override
  void dispose() {
    is_finish = true;
    stop_all();
    list_square_blue = [];
    list_square_orange = [];

    super.dispose();
  } //  Race race;

  bool isLoadingSubmitted = false;

//  int indexLoop;

  TextEditingController cont_time = new TextEditingController();

  _RecordState(this.event_l, this.race);

  Future<void> getLapsTimes() async {
    lapsList = [];
    await DBProvider.db.Course_list_all(event_l).then((value) {
      if (value.length > 0) {
        setState(() {
//          print("CCCCC>>>>> ${value}");
          lapsList = value[0].coursepoints;
          list_search = value[0].coursepoints;
          list_search_temp = value[0].coursepoints;
          lapsList.insert(0, new Coursepoints(lapIndex: -9, label: 'Show all'));
//          print("lapsList>>> ${lapsList}");
//          list_course=value;
        });
      }
    });

//    lapsList=event_l.;
//    print(race.jso['splitNames']);
  }

  Future<void> get_course() async {
    DetailsTracking detailsTracking_new;
    await DBProvider.db
        .DetailsTracking_limit1(race)
        .then((value) => detailsTracking_new = value);
    await DBProvider.db
        .Course_list_from_id(detailsTracking_new.id)
        .then((value) {
      course = value[0];
    });
  }

  @override
  void initState() {
//    startTimeout();
//    lapsList.add();
//    delay();
//    update_for_finish_ssqure_orange();

    list_square_grey_io = [];
    load_and_refresh_from_server();
    update_for_finish_square_orange();
//    set_timer_old_square();

    super.initState();
  }

  Future<void> load_and_refresh_from_server() async {
    try {
      await DBProvider.db.get_trackings_race(race, event_l, is_update: true);

      await load_race();
      await start_all();
      await getLapsTimes();
      await load_square_list();
      await get_course();
      setState(() {
        cpmplite_load = true;
      });
    } catch (exxx) {
//      print("EEEEXX ${exxx}");
      await load_race();
      await start_all();

      await getLapsTimes();
      await load_square_list();

      setState(() {
        cpmplite_load = true;
      });
    }
    await start_all();
  }

  Future<void> load_race() async {
    await DBProvider.db.Race_list_from_event(event_l).then((value) {
      setState(() {
        lst_race = value;
      });
    });
  }

  Widget endRace() {
//    print("lst_race>>> ${lst_race}");
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff121214),
                  height: 60,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.blue,
                      ),
                      Text(
                        "End Race",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff121214),
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Do you want to end the races? You cantundo this.",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: InkWell(
                  onTap: () {
                    endRaceFunc(lst_race);
                  },
                  child: isLoadingSubmitted
                      ? SpinKitThreeBounce(
                          color: Colors.red,
                          size: 17,
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          width: 250,
                          height: 50,
                          child: Center(
                            child: Text(
                              "END RACES",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    width: 250,
                    height: 50,
                    child: Center(
                      child: Text(
                        "KEEP RACES ON ",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 16.0, top: 16.0),
          child: Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: Color(0xff121214),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> finish_race(Race race) async {
//    print(race.toString());
    try {
      await http
          .post(
              'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/finish')
          .then((data) async {
//      races = [];
        Map<String, dynamic> dataServer = jsonDecode(data.body);
//print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ${dataServer}");
        if (dataServer["status"] == "finished") {
//
          race.is_start = false;
          TrackingInformation trackingInformation =
              new TrackingInformation.fromJSONSERVER(dataServer, race, event_l);
          await DBProvider.db
              .insert_tracking(trackingInformation)
              .then((value) {
//            DBProvider.db.update_race(race);
          });

          return;
        }
//        startRace(race,false);
      });
    } catch (ex) {
//      print("<<<<<<<WW ${ex}");
    }
  }

  endRaceFunc(List<Race> lst_race) async {
    lst_race.forEach((element) async {
      List<TrackingInformation> list_tracking = new List();
      await DBProvider.db
          .TrackingInformation_list_from_race(element)
          .then((value) {
        setState(() {
          list_tracking = value;
//          print("list_tracking>>>> ${list_tracking}");
        });
      });
      list_tracking.forEach((el) async {
//        print("EL>>>> ${el}");
        if (el.status == "guned") {
          await finish_race(element);
        }
      });
    });
    Navigator.pop(context);
    setState(() {});
  }

//  startTimeout([int milliseconds]) {
//    var duration = interval;
//    Timer.periodic(duration, (timer) {
//      setState(() {
//        print(timer.tick);
//        currentSeconds = timerMaxSeconds - timer.tick;
//        cont_time.text = currentSeconds.toString();
//        if (timer.tick >= timerMaxSeconds) timer.cancel();
//      });
//    });
//  }

  @override
  Widget build(BuildContext context) {
//    delay();
//    set_timer_old_square();

    return Scaffold(
      backgroundColor: Colors.black,
//      floatingActionButton: FloatingActionButton(
//        onPressed: () async {
//          DetailsTracking detailsTracking_new;
//          await DBProvider.db
//              .DetailsTracking_limit1(race)
//              .then((value) => detailsTracking_new = value);
//          detailsTracking_new.lst_timings.forEach((element) {
//            print("elf>>>>> ${element.bib} ${element.is_start}");
//          });
//        },
//      ),
      key: _scaffoldKey,
      body: cpmplite_load
          ? SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 16.0, left: 8.0, right: 8.0, bottom: 8.0),
                    child: Container(
                      color: Color(0xff121214),
                      child: ExpansionTile(
                        title: Container(
                          // width: MediaQuery.of(context).size.width,
                          height: 40,
                          color: Color(0xff121214),
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: list_search
                                        .where((element) =>
                                            element.lapIndex != -9 &&
                                            element.reference !=
                                                course.coursepoints.first
                                                    .reference)
                                        .map((e) => e.reference ==
                                                course
                                                    .coursepoints.last.reference
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 4),
                                                child: Text(
                                                  "Målgång ${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              )
                                            : Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 4),
                                                child: Text(
                                                  "${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} Km ",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ))
                                        .toList(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 1.1,
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Column(
                                    children: lapsList
                                        .where((element) =>
                                            element.reference !=
                                            course.coursepoints.first.reference)
                                        .map((e) => Padding(
                                              padding:
                                                  const EdgeInsets.only(top: 4),
                                              child: InkWell(
                                                onTap: () {},
                                                child: Stack(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 35,
                                                      child: ListTile(
                                                        leading: e.reference ==
                                                                course
                                                                    .coursepoints
                                                                    .last
                                                                    .reference
                                                            ? Text(
                                                                "Målgång (${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))}km)",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                    font_size_body,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              )
                                                            : Text(
                                                                "${e.label}  ${e.distance != null ? '(' + removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1))) + 'Km)' : ''} ",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                    font_size_body,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              ),
                                                        trailing: Checkbox(
                                                          onChanged:
                                                              (val) async {
                                                            e.select = val;
                                                            if (lapsList[0]
                                                                    .select ==
                                                                true) {
                                                              await fill_search_list(
                                                                  list_search_temp
                                                                      .toList());
                                                            } else {
                                                              await fill_search_list(lapsList
                                                                  .where((element) =>
                                                                      element
                                                                          .select ==
                                                                      true)
                                                                  .toList());
                                                            }
                                                            load_square_list();
                                                          },
                                                          value: e.select,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ))
                                        .toList(),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 24.0, left: 4.0),
                                    child: InkWell(
                                      onTap: () {
                                        _scaffoldKey.currentState
                                            .showBottomSheet(
                                                (context) => endRace(),
                                                backgroundColor: Colors.black);
                                      },
                                      child: Text(
                                        "End Race",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.start,
                          children: list_square_orange
                              .where((element) => !element.is_finish)
                              .map((e) => e.currentSeconds != 0
                                  ? TranslationAnimatedWidget.tween(
                                      enabled: e.start_animation,
                                      translationDisabled: Offset(0, 0),
                                      translationEnabled: Offset(
                                          -MediaQuery.of(context).size.width,
                                          0),
                                      child: OpacityAnimatedWidget.tween(
                                          enabled: e.start_animation,
                                          opacityDisabled: 1,
                                          opacityEnabled: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Listener(
                                              onPointerDown: (details) {
//                          _buttonPressed = true;
//                          _increaseCounterWhilePressed();
                                                _timer = new Timer(
                                                    Duration(seconds: 3),
                                                    () async {
                                                  _timer.cancel();
                                                  await stop_all()
                                                      .then((value) async {
                                                    await _showMyDialogforUndo(
                                                            e.time_in_mill,
                                                            e.bib)
                                                        .then((value) {
                                                      start_all();
                                                    });

                                                    /**********Alert dialaog*********/

//                                            Navigator.push(
//                                                context,
//                                                new MaterialPageRoute(
//                                                    builder: (context) =>
//                                                        UndoPage(
//                                                            race,
//                                                            event_l,
//                                                            list_square_orange[
//                                                                    index]
//                                                                .bib))).then(
//                                                (value) async {
//                                              await load_square_list_refresh();
//                                              await start_all();
//                                            });
                                                  });
                                                });
                                                /***************code for thread and update******************/
                                              },
                                              child: Container(
                                                child: Center(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 6),
                                                    child:
                                                        SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.vertical,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              Text(
                                                                get_timing(e),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        17,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
//                                                      Padding(
//                                                        padding:
//                                                            const EdgeInsets
//                                                                    .only(
//                                                                left: 3.0),
//                                                        child: Text(
//                                                          list_square_orange[
//                                                                  index]
//                                                              .txt,
//                                                          style: TextStyle(
//                                                              color:
//                                                                  Colors.white,
//                                                              fontSize: 11,
//                                                              fontWeight:
//                                                                  FontWeight
//                                                                      .bold),
//                                                        ),
//                                                      ),
                                                            ],
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            child: Center(
                                                              child: Text(
                                                                e.time_in_mill !=
                                                                        null
                                                                    ? '${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).hour}:${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).minute}:${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).second}'
                                                                    : '',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        10,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                width: 70,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                        colors: <Color>[
                                                          Colors.orange,
                                                          Colors.orange,
                                                          Colors.orange,
                                                          Colors.orange,
                                                          Colors.deepOrange,
                                                          Colors.deepOrange
                                                        ],
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                              ),
                                            ),
                                          )))
                                  : Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Listener(
                                        onPointerDown: (details) {
//                          _buttonPressed = true;
//                          _increaseCounterWhilePressed();
                                          _timer = new Timer(
                                              Duration(seconds: 2), () async {
                                            _timer.cancel();
                                            await stop_all()
                                                .then((value) async {
                                              Navigator.push(
                                                  context,
                                                  new MaterialPageRoute(
                                                      builder: (context) =>
                                                          ShowInformationRacer(
                                                              race,
                                                              event_l,
                                                              e))).then(
                                                  (value) async {
                                                await load_square_list_refresh()
                                                    .then((value) async {
                                                  await start_all();
                                                });
                                              });
                                            });
                                          });
                                        },
                                        onPointerUp: (details) async {
                                          _timer.cancel();
//                                        await stop_all();
                                          await finish_racer(
                                              e.course.coursepoints.firstWhere(
                                                  (element) =>
                                                      element.reference ==
                                                      e.lst_timings.last
                                                          .courspoint),
                                              e);
                                        },
                                        child: Container(
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.only(top: 6),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Text(
                                                        get_timing(e),
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 17,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
//                                                        Padding(
//                                                          padding:
//                                                              const EdgeInsets
//                                                                      .only(
//                                                                  left: 3.0),
//                                                          child: Text(
//                                                            get_timing(list_square_orange[
//                                                                        index]) !=
//                                                                    null
//                                                                ? list_square_orange[
//                                                                        index]
//                                                                    .txt
//                                                                : 'ss',
//                                                            style: TextStyle(
//                                                                color: Colors
//                                                                    .white,
//                                                                fontSize: 11,
//                                                                fontWeight:
//                                                                    FontWeight
//                                                                        .bold),
//                                                          ),
//                                                        ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          width: 70,
                                          height: 50,
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  colors: <Color>[
                                                    Colors.orange,
                                                    Colors.orange,
                                                    Colors.orange,
                                                    Colors.orange,
                                                    Colors.deepOrange,
                                                    Colors.deepOrange
                                                  ],
                                                  begin: Alignment.topLeft,
                                                  end: Alignment.bottomRight),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                        ),
                                      ),
                                    ))
                              .toList()),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.start,
                          children: list_square_blue
                              .where((element) => !element.is_finish)
                              .map((e) => e.currentSeconds != 0
                                  ? TranslationAnimatedWidget.tween(
                                      enabled: e.start_animation,
                                      translationDisabled: Offset(0, 0),
                                      translationEnabled: Offset(
                                          -MediaQuery.of(context).size.width,
                                          0),
                                      child: OpacityAnimatedWidget.tween(
                                          enabled: e.start_animation,
                                          opacityDisabled: 1,
                                          opacityEnabled: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Listener(
                                              onPointerDown: (details) {
//                          _buttonPressed = true;
//                          _increaseCounterWhilePressed();
                                                _timer = new Timer(
                                                    Duration(seconds: 3),
                                                    () async {
                                                  _timer.cancel();
                                                  await stop_all()
                                                      .then((value) async {
                                                    await _showMyDialogforUndo(
                                                            e.time_in_mill,
                                                            e.bib,
                                                            is_orange: false)
                                                        .then((value) async {
                                                      await start_all();
                                                    });
                                                    /**********Alert dialaog*********/

//                                            Navigator.push(
//                                                context,
//                                                new MaterialPageRoute(
//                                                    builder: (context) =>
//                                                        UndoPage(
//                                                            race,
//                                                            event_l,
//                                                            list_square_orange[
//                                                                    index]
//                                                                .bib))).then(
//                                                (value) async {
//                                              await load_square_list_refresh();
//                                              await start_all();
//                                            });
                                                  });
                                                });
                                                /***************code for thread and update******************/
                                              },
                                              child: Container(
                                                child: Center(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 6),
                                                    child:
                                                        SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      child: Column(
                                                        children: <Widget>[
                                                          SingleChildScrollView(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  get_timing(e),
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontSize:
                                                                          17,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold),
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      top: 4),
                                                                  child: Text(
                                                                    '${e.km != 0 ? removeDecimalZeroFormat(double.parse((e.km / 1000).toStringAsFixed(1))) : ''}',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            10,
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                )
//                                                      Padding(
//                                                        padding:
//                                                       li     const EdgeInsets
//                                                                    .only(
//                                                                left: 3.0),
//                                                        child: Text(
//                                                          list_square_orange[
//                                                                  index]
//                                                              .txt,
//                                                          style: TextStyle(
//                                                              color:
//                                                                  Colors.white,
//                                                              fontSize: 11,
//                                                              fontWeight:
//                                                                  FontWeight
//                                                                      .bold),
//                                                        ),
//                                                      ),
                                                              ],
                                                            ),
                                                            scrollDirection:
                                                                Axis.horizontal,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            child: Center(
                                                              child: Text(
                                                                e.time_in_mill !=
                                                                        null
                                                                    ? '${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).hour}:${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).minute}:${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).second}'
                                                                    : '',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        10,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                width: 70,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                    color: Colors.blue,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                              ),
                                            ),
                                          )),
                                    )
                                  : e.currentSeconds == 0
                                      ? Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Listener(
                                            onPointerUp: (da) async {
                                              _timer.cancel();
                                              await stop_all()
                                                  .then((value) async {
                                                await finish_racer(
                                                        e.course.coursepoints
                                                            .firstWhere((element) =>
                                                                element
                                                                    .reference ==
                                                                e
                                                                    .lst_timings
                                                                    .last
                                                                    .courspoint),
                                                        e,
                                                        is_orange_list: false)
                                                    .then((value) {
//                                                new Timer.periodic(Duration(milliseconds: 10), (t){
//
//                                                  setState(() {
//
//                                                  });
//                                                }).cancel();
                                                });
                                              });
                                            },
                                            onPointerDown: (de) {
                                              _timer = new Timer(
                                                  Duration(seconds: 3),
                                                  () async {
                                                await stop_all();

                                                _timer.cancel();
                                                Navigator.push(
                                                    context,
                                                    new MaterialPageRoute(
                                                        builder: (context) =>
                                                            ShowInformationRacer(
                                                                race,
                                                                event_l,
                                                                e))).then(
                                                    (value) async {
                                                  await load_square_list_refresh()
                                                      .then((value) async {
                                                    await start_all();
                                                  });
                                                });
                                              });
                                            },
                                            child: Container(
                                              child: Center(
                                                child: Column(mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Text(
                                                            get_timing(e),
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 17,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 2,
                                                                    top: 4),
                                                            child: Text(
                                                              '${e.km != 0 ? removeDecimalZeroFormat(double.parse((e.km / 1000).toStringAsFixed(1))) : ''}',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 10,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Text(
                                                      '${e.lst_timings.length}/${course.coursepoints.length}',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              width: 70,
                                              height: 50,
                                              decoration: BoxDecoration(
                                                  color: Colors.blue,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10))),
                                            ),
                                          ),
                                        )
                                      : Container())
                              .toList()),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 8.0, top: MediaQuery.of(context).size.height / 9),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        children: list_square_grey_io
                            .map((e) => Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Listener(
                                    onPointerUp: (da) {
                                      _timer.cancel();
                                    },
                                    onPointerDown: (de) {
                                      _timer = new Timer(Duration(seconds: 3),
                                          () async {
                                        await _showMyDialogforUundifind(
                                          e.time_in_mill,
                                          e,
                                        );
                                      });
                                    },
                                    child: Container(
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            SingleChildScrollView(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 2),
                                                child: Center(
                                                  child: Text(
                                                    e.txt,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                              ),
                                              scrollDirection: Axis.horizontal,
                                            ),
                                            e.time_in_mill != null
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 0),
                                                    child: Text(
                                                      '${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).hour}:${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).minute}:${DateTime.fromMillisecondsSinceEpoch(e.time_in_mill).second} ',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  )
                                                : Container(),
                                            Text(
                                              "Hold 3 sec",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                      ),
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 8.0, top: MediaQuery.of(context).size.height / 9),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          children: list_square_grey
                              .map((e) => Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                      onTap: () async {
                                        int count = 0;
                                        for (int i = 1;
                                            i < list_square_grey_io.length;
                                            i++) {
                                          print('i>>>>>${i}');
                                          if (list_square_grey_io.firstWhere(
                                                  (element) =>
                                                      element.count_gray == i,
                                                  orElse: () => null) ==
                                              null) {
                                            count = i;
                                            list_square_grey_io.add(new Square(
                                                txt: "OI ${count}",
                                                count_gray: count,
                                                time_in_mill: DateTime.now()
                                                    .millisecondsSinceEpoch));
                                            list_square_grey_io.sort((b, a) => b
                                                .count_gray
                                                .compareTo(a.count_gray));
                                            setState(() {});
                                            return;
                                          }
                                        }

                                        list_square_grey_io.add(new Square(
                                            txt:
                                                "OI ${list_square_grey_io.length + 1}",
                                            count_gray:
                                                list_square_grey_io.length + 1,
                                            time_in_mill: DateTime.now()
                                                .millisecondsSinceEpoch));
                                        list_square_grey_io.sort((b, a) => b
                                            .count_gray
                                            .compareTo(a.count_gray));
                                        setState(() {});
//                                  var coursed;
//                                  var details;
//                                  await DBProvider.db
//                                      .DetailsTracking_limit1(race)
//                                      .then((value) {
//                                    details = value;
//                                  });
//                                  await DBProvider.db
//                                      .Course_list_from_id(details.id)
//                                      .then((value) {
//                                    coursed = value[0];
//                                  });
//                                  Navigator.push(
//                                      context,
//                                      new MaterialPageRoute(
//                                          builder: (context) =>
//                                              UnidentifiedWithoutNote(
//                                                  event_l,
//                                                  details,
//                                                  coursed,
//                                                  race))).then((value) {
//                                    load_square_list_refresh();
//                                  });
                                      },
                                      child: Container(
                                        child: Center(
                                          child: Text(
                                            e.txt,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        width: 70,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                      ),
                                    ),
                                  ))
                              .toList()),
                    ),
                  ),
                ],
              ),
            )
          : spinKitThreeBounce,
    );
  }

  void finishLoop() {
//    DBProvider.db.Course_list_from_id(detailstracking.id).then((value) {
//      setState(() {
////        list_square_blue.clear();
////        value_filter = value[0].coursepoints.last;
//
////        load_square_list();
//      });
//    });
  }

  String get_timing(Square square) {
    square.lst_timings.sort((a, b) =>
        get_coursepoint(a.courspoint, square.course)
            .compareTo(get_coursepoint(b.courspoint, square.course)));
    return square.lst_timings.last.bib.toString();
  }

//
//  Future<void> delay() async {
//    await new Future.delayed(new Duration(seconds: 1), () {
//      setState(() {});
//      if (!is_finish) {
//        delay();
//      }
//    });
//    return;
//  }

  Future<void> update_for_finish_square_orange() async {
    await Future.delayed(new Duration(seconds: 1), () async {
//      bool is_change = false;
      List<Square> list_all = new List();
      list_all.addAll(list_square_orange);
//      list_square_orange
//          .where((elssq) => elssq.is_finish && elssq.is_start_from_page)
//          .forEach((element) async {
//        is_change = await enable_timer_square_orange(is_change, element)
//            .then((value) => true);
//      });
      list_all.addAll(list_square_blue);
//      list_square_blue
//          .where((elssq) => elssq.is_finish && elssq.is_start_from_page)
//          .forEach((element) async {
//        is_change = await enable_timer_square_orange(is_change, element,
//                is_orange_list: false)
//            .then((value) => true);
//      });
      list_all
          .where((element) => element.is_finish && element.is_start_from_page)
          .forEach((element) async {
        print("elch>>>>>>> ${element.currentSeconds}");
        if (list_square_orange.firstWhere((elor) => elor.bib == element.bib,
                orElse: () => null) !=
            null) {
          await enable_timer_square_orange(element, is_orange_list: true);
        } else if (list_square_blue.firstWhere(
                (elor) => elor.bib == element.bib,
                orElse: () => null) !=
            null) {
          await enable_timer_square_orange(element, is_orange_list: false);
        }
        if (list_square_orange
                    .where((element) =>
                        element.is_start && element.is_start_from_page)
                    .length ==
                0 &&
            list_square_blue
                    .where((element) =>
                        element.is_start && element.is_start_from_page)
                    .length ==
                0) {
          print('lats>>>ffffff>se');
          setState(() {});
        }
      });

      if (!is_finish) {
        await update_for_finish_square_orange();
      }
//      return;
    });

    return;
  }

  Future<bool> enable_timer_square_orange(Square square,
      {bool is_orange_list = true}) async {
    bool is_change = false;
//    print('squareforsend>>>>> ${square.toString()}');
    DetailsTracking detailsTracking_new;
    await DBProvider.db.DetailsTracking_limit1(race).then((value) async {
      detailsTracking_new = value;
    }).then((value) async {
      List lst_timings = await detailsTracking_new.timings['timings'];
      var eltim = lst_timings.firstWhere((element) =>
          element.containsKey("temp") && element['bib'] == square.bib);
      Map<String, dynamic> bodys = eltim['temp'];
      Map<String, dynamic> bodys_new = {
        "timeMs": bodys['timeMs'],
        "bib": bodys['bib'],
        "coursepoint": bodys['courspoint'],
        "source": bodys['source'],
        "participantRef": bodys['participantRef']
      };

      await http.put(
          'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/timings',
          body: jsonEncode(bodys_new),
          headers: <String, String>{
            'Content-Type': 'application/json;charset=UTF-8'
          }).then((data) async {
//          print("loadingsend>>>>>>>>");
        //                print("send>>>>>>>>>>>>>>>>>");
        Map<String, dynamic> js = jsonDecode(data.body);
        if (js != null) {
//            print("js>>>>> ${js}");
//            print("before>d>> ${eltim}");
//
//            print("after>d>> ${eltim}");
//            is_change = true;
          if (is_orange_list) {
//              print(
//                  "beforeUp>>>>>>>>>>>>>>>>>> ${lst_timings[lst_timings.indexOf(eltim)]}");
            lst_timings[lst_timings.indexOf(eltim)] = js;
//              print(
//                  'afterUp>>>>>>>>>>>>>>>>>> ${lst_timings[lst_timings.indexOf(js)]}');
          } else {
            lst_timings.add(js);
          }

          await detailsTracking_new.timings
              .update('timings', (value) => lst_timings);
          await DBProvider.db.update_detailstracking(detailsTracking_new);
          await DBProvider.db.DetailsTracking_limit1(race).then((value) async {
            detailsTracking_new = value;
          }).then((value) async {
            if (is_orange_list) {
              print("hereprob>>>>>>>>>>>>>>>>> ${square.toString()}");

              list_square_orange
                  .removeWhere((element) => element.bib == square.bib);
              is_change = true;
              return is_change;
//             setState(() {
//          ;
//             });
//              setState(() {
//
//              });
            } else {
//              print("hereprob>>>>>>>>>>>>>>>>> ${square.toString()}");
              list_square_blue
                  .removeWhere((element) => element.bib == square.bib);
              is_change = true;
              return is_change;
//              setState(() {         });
            }
          });
        }
      });
    });

    return is_change;
  }

  Future<void> send_undifind(Map<String, dynamic> bodys, Square squareg) async {
    DetailsTracking detailsTracking_new;

    await DBProvider.db
        .DetailsTracking_limit1(race)
        .then((value) => detailsTracking_new = value)
        .then((value) async {
      List lst_timings = detailsTracking_new.timings['timings'];

      try {
        await http.put(
            'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/timings',
            body: jsonEncode(bodys),
            headers: <String, String>{
              'Content-Type': 'application/json;charset=UTF-8'
            }).then((data) async {
          print("loadingsend>>>>>>>>");
          //                print("send>>>>>>>>>>>>>>>>>");
          Map<String, dynamic> js = jsonDecode(data.body);
          if (js != null) {
            print('js>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${js}');

            lst_timings.add(js);

            Course course;

            await detailsTracking_new.timings
                .update('timings', (value) => lst_timings);
            await DBProvider.db.update_detailstracking(detailsTracking_new);

            await DBProvider.db.DetailsTracking_limit1(race).then((value) {
              detailsTracking_new = value;
            });
            await DBProvider.db
                .Course_list_from_id(detailsTracking_new.id)
                .then((value) {
              course = value[0];
            });
            detailsTracking_new.lst_timings.forEach((element) {
              print('element>>>>>>>>>>>>>>>${element}');
            });

            Square square = new Square(
                course: course,
                function: () {
                  setState(() {});
                },
                detailsTracking: detailsTracking_new,
                bib: js['bib'],
                lst_timings: detailsTracking_new.lst_timings
                    .where((el) => el.bib == js['bib'])
                    .toList(growable: true),
                is_start_from_page: false,
                time_in_mill: detailsTracking_new.lst_timings
                            .lastWhere((el) => el.bib == js['bib'])
                            .temp_time !=
                        null
                    ? lst_timings
                        .lastWhere((el) => el.bib == js['bib'])
                        .temp_time
                    : null,
                timerMaxSeconds: 12);
            print('square>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${square}');
            square.lst_timings.sort((a, b) =>
                get_coursepoint(a.courspoint, course)
                    .compareTo(get_coursepoint(b.courspoint, course)));

//            lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
//                .compareTo(get_coursepoint(b.courspoint, course)));
//            square.lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
//                .compareTo(get_coursepoint(b.courspoint, course)));
            if (square.lst_timings.last.courspoint ==
                course.coursepoints.last.reference) {
              list_square_orange.add(square);
            } else {
              square.km = list_search
                  .firstWhere(
                      (element) =>
                          element.reference ==
                          get_coursepoint(
                                  square.lst_timings.last.courspoint, course)
                              .reference,
                      orElse: () => null)
                  .distance;
              list_square_blue.add(square);
            }
            list_square_grey_io
                .removeWhere((element) => element.bib == squareg.bib);
            setState(() {});
//                .then((value) => detailsTracking_new = value)
//                .then((value) async {
//              lst_timings = detailsTracking_new.timings['timings'];
//            });
          }
        });
      } catch (ex) {
        print('Exxxxxxxxxxxxxxxxxxxxx>>> ${ex}');
        Toast.show(ex.toString(), context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
      }
    });
  }

//  Future<void> _showMyDialog(Square square) async {
//    DateTime temp;
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return AlertDialog(
//          backgroundColor: Colors.black,
//          title: Text('Set Time'),
//          content: SingleChildScrollView(
//            scrollDirection: Axis.vertical,
//            child: TimePickerSpinner(
//              normalTextStyle: TextStyle(fontSize: 24, color: Colors.grey),
//              highlightedTextStyle:
//                  TextStyle(fontSize: 24, color: Colors.white),
//              isShowSeconds: true,
//              is24HourMode: false,
//              spacing: 10,
//              itemHeight: 60,
//              isForce2Digits: true,
//              onTimeChange: (time) {
//                setState(() {
//                  temp = time;
//                });
//              },
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('Cancel'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//            FlatButton(
//              child: Text('Ok'),
//              onPressed: () {
//                if (temp != null) {}
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }

  Future<void> finish_racer(Coursepoints coursepoints, Square square,
      {bool is_orange_list = true}) async {
    print(">>>>>>>>>>>>> ${coursepoints.reference}");
    Map<String, dynamic> dataServer = {
      'temp': {
        "timeMs": DateTime.now().millisecondsSinceEpoch,
        "bib": square.lst_timings.last.bib,
        "courspoint": is_orange_list
            ? coursepoints.reference
            : square
                .course
                .coursepoints[square.course.coursepoints.indexWhere(
                      (element) => element.reference == coursepoints.reference,
                    ) +
                    1]
                .reference,
        "source": "manual",
        "participantRef": square.lst_timings.last.participantRef,
      }
    };
//print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ${dataServer}");
    if (dataServer != null) {
//print
      DetailsTracking detailsTracking_new;
      await DBProvider.db
          .DetailsTracking_limit1(race)
          .then((value) => detailsTracking_new = value);

      List list_timings = detailsTracking_new.timings['timings'];
      list_timings
          .where((elt) =>
              elt['bib'] == square.bib &&
              elt['courspoint'] == coursepoints.reference)
          .forEach((el) {
        Map<String, dynamic> te = el;
//        print("SERARCH>>>> ${el}");
        te.addAll(dataServer);
        te.addAll({'is_start': true});
        el = te;
//        print(">>>>>>>EL ${el}");
//        print(">>>>>>>TE ${te}");
      });

//      temp
//      print("before<>>>   ${square.detailsTracking.timings['timings']}");
      await detailsTracking_new.timings
          .update('timings', (value) => list_timings);
//        square.detailsTracking.timings = temp;
//      print("after<>>>   ${square.detailsTracking.timings['timings']}");
      await DBProvider.db
          .update_detailstracking(detailsTracking_new)
          .then((value) async {
//        print("here>>>>>>1");
        if (is_orange_list) {
          list_square_orange
              .where((element) => element.bib == square.bib)
              .forEach((element) async {
            if (!square.is_finish && !square.is_start_from_page) {
//              print("here>>>>>>2");

              square.is_start_from_page = true;

              square.time_in_mill = dataServer['temp']['timeMs'];

              await start_all();

//            return;
            }
          });
        } else {
          list_square_blue
              .where((element) => element.bib == square.bib)
              .forEach((element) async {
            if (!square.is_finish && !square.is_start_from_page) {
              print("here>>>>>>2");
              square.is_start_from_page = true;

              square.time_in_mill = dataServer['temp']['timeMs'];
              await start_all();

//            return;
            }
          });
        }

//        await   load_square_list_refresh();
        Toast.show("Complete", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);

//        setState(() {});
      });

//          return;
    }
  }

  Future<void> _showMyDialogforUndo(int mili, int bib,
      {bool is_orange = true}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.black,
          title: Text('Do You want Undo?',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w100)),
          content: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Bib: ",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w100),
                      ),
                      Text(
                        bib.toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w100),
                      )
                    ],
                  ),
                ),
                Container(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Row(
                        children: <Widget>[
                          Text("Time: ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w100)),
                          Text(
                              '${DateTime.fromMillisecondsSinceEpoch(mili).hour}:${DateTime.fromMillisecondsSinceEpoch(mili).minute}:${DateTime.fromMillisecondsSinceEpoch(mili).second}',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w100))
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                if (!is_orange) {
                  if (list_square_blue.firstWhere(
                              (element) => element.bib == bib,
                              orElse: () => null) !=
                          null &&
                      list_square_blue
                          .firstWhere((element) => element.bib == bib,
                              orElse: () => null)
                          .start_animation) {
                    Toast.show(' Sorry I cant hold back ', context);
                    Navigator.pop(context);
                    return;
                  }
                } else {
                  if (list_square_orange.firstWhere(
                              (element) => element.bib == bib,
                              orElse: () => null) !=
                          null &&
                      list_square_orange
                          .firstWhere((element) => element.bib == bib,
                              orElse: () => null)
                          .start_animation) {
                    Toast.show(' Sorry I cant hold back ', context);
                    Navigator.pop(context);
                    return;
                  }
                }
                /*********************if remove from blue and orange*****************************/
                if (list_square_blue.firstWhere((element) => element.bib == bib,
                            orElse: () => null) ==
                        null &&
                    list_square_blue.firstWhere((element) => element.bib == bib,
                            orElse: () => null) ==
                        null) {
                  Toast.show(' Sorry I cant hold back ', context);
                  Navigator.pop(context);
                  return;
                }
                await undo(bib, is_orange: is_orange).then((value) {
//                  load_square_list_refresh();
                });
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> undo(int bibp, {bool is_orange = true}) async {
    DetailsTracking detailsTracking_new;
    await DBProvider.db
        .DetailsTracking_limit1(race)
        .then((value) => detailsTracking_new = value);

    List list_timings = detailsTracking_new.timings['timings'];

    var el = list_timings.firstWhere(
        (element) => element.containsKey("temp") && element['bib'] == bibp,
        orElse: () => null);
    if (el == null) {
      return;
    }
    el.remove("temp");
//      el.remove("is_start");
    await detailsTracking_new.timings
        .update('timings', (value) => list_timings);
    await DBProvider.db
        .update_detailstracking(detailsTracking_new)
        .then((value) async {
//            list_square_orange.where((element) => element.bib==bibp).forEach((element) {
//
//              element.is_start_from_page=false;
//              element.timerMaxSeconds=10;
//              element.is_start=false;
//            });
      if (is_orange) {
        list_square_orange
            .where((element) => element.bib == bibp)
            .forEach((element) async {
          element.is_start_from_page = false;
          await element.cancel();

          element.currentSeconds = 0;
          element.interval = Duration(seconds: 1);
          element.increase = Duration(seconds: 0);
          element.is_stop = false;
          element.time_in_mill = null;

//            element.is_delete = true;
//            element.increase =  Duration(seconds: 1);
          setState(() {});
        });
      } else {
        list_square_blue
            .where((element) => element.bib == bibp)
            .forEach((element) async {
          element.is_start_from_page = false;
          await element.cancel();

          element.currentSeconds = 0;
          element.interval = Duration(seconds: 1);
          element.increase = Duration(seconds: 0);
          element.is_stop = false;
          element.time_in_mill = null;
          element.is_start_from_page = false;
          element.is_start = false;
          element.currentSeconds = 0;
          element.interval = Duration(seconds: 1);
          element.increase = Duration(seconds: 0);
//            element.is_delete = true;
//            element.increase =  Duration(seconds: 1);
          setState(() {});
        });
//          setState(() {});
//

      }

      Toast.show("Complete", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//        setState(() {});

//        return;
    });
//  Navigator.pop(context);
  }

  Future<void> _showMyDialogforUundifind(
      int time_in_mili, Square squareg) async {
    print("gray>>>>> ${squareg.toString()}");
    lapsList_for_undifind = [];
    lapsList_for_undifind = list_search
        .where((element) => element.label != 'Show all')
        .map((e) => new Coursepoints(
            distance: e.distance,
            label: e.label,
            lapIndex: e.lapIndex,
            type: e.type,
            reference: e.reference))
        .toSet()
        .toList();

    TextEditingController _controller_bib = new TextEditingController();
    if (squareg.map_gray != null && squareg.map_gray.containsKey('bib')) {
      _controller_bib.text = squareg.map_gray['bib'].toString();
    }

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.black,
//              Text(
//                "Bib: ",
//                style: TextStyle(
//                    color: Colors.white,
//                    fontSize: 20,
//                    fontWeight: FontWeight.w100),
//              )

          content: StatefulBuilder(
            builder: (cont, StateSetter setter) {
              return SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Text("Bib:",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400)),
                      Flexible(
                        child: TextField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(7),
                            WhitelistingTextInputFormatter.digitsOnly,
                            BlacklistingTextInputFormatter.singleLineFormatter,
                          ],
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding: EdgeInsets.only(
                                left: 15, bottom: 11, top: 11, right: 15),
                          ),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400),
                          controller: _controller_bib,
                        ),
                      )
                    ]),
                    Text(
                        "Recorded time : '${DateTime.fromMillisecondsSinceEpoch(time_in_mili).hour}:${DateTime.fromMillisecondsSinceEpoch(time_in_mili).minute}:${DateTime.fromMillisecondsSinceEpoch(time_in_mili).second}'",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.w400)),
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                      child: Container(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Text("Timing position choose one:",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400)),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Column(
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: lapsList_for_undifind
                            .where((element) =>
                                element.reference !=
                                course.coursepoints.first.reference)
                            .map((e) => Padding(
                                  padding: const EdgeInsets.only(left: 4),
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 0.0),
                                          child: e.reference ==
                                                  course.coursepoints.last
                                                      .reference
                                              ? Text(
                                                  "Målgång ${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                          FontWeight.w400),
                                                )
                                              : Text(
                                                  "${e.distance != null ? removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1))) : ''} Km",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                          FontWeight.w400),
                                                ),
                                        ),
                                        Theme(
                                          data: Theme.of(context).copyWith(
                                              unselectedWidgetColor:
                                                  Colors.white),
                                          child: Checkbox(
//                                        checkColor: Colors.white,
                                            onChanged: (val) async {
                                              lapsList_for_undifind
                                                  .forEach((element) {
                                                element.select = false;
                                              });
                                              setter(() => e.select = val);
                                            },
                                            value: e.select,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ))
                            .toList()),
                  ],
                ),
              );
            },
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Dont Save',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: font_size_body,
                      fontWeight: FontWeight.w300)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Save',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: font_size_body,
                      fontWeight: FontWeight.w300)),
              onPressed: () async {
                if (lapsList_for_undifind
                        .where((element) => element.select == true)
                        .length ==
                    0) {
                  if (_controller_bib.text.isNotEmpty) {
                    list_square_grey_io
                        .where((element) =>
                            element.count_gray == squareg.count_gray)
                        .forEach((element) {
                      element.map_gray = {
                        'bib': int.parse(_controller_bib.text)
                      };
                      element.bib = int.parse(_controller_bib.text);
                      squareg = element;
                    });
                  }
                } else if (_controller_bib.text.isNotEmpty) {
                  Map<String, dynamic> jso = {
                    "timeMs": time_in_mili,
                    "bib": int.parse(_controller_bib.text),
                    "coursepoint": lapsList_for_undifind
                        .where((element) => element.select == true)
                        .toList()[0]
                        .reference,
                    "source": "manual",
                    "participantRef": 'unknow'
                  };
                  squareg.bib = int.parse(_controller_bib.text);
                  if (list_square_blue.firstWhere(
                              (element) => element.bib == squareg.bib,
                              orElse: () => null) !=
                          null &&
                      list_square_blue
                          .firstWhere((element) => element.bib == squareg.bib,
                              orElse: () => null)
                          .start_animation) {
                    Navigator.pop(context);
                    return;
                  }

                  await send_undifind(jso, squareg);
                }
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text('Delete',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: font_size_body,
                      fontWeight: FontWeight.w300)),
              onPressed: () async {
                list_square_grey_io
                    .removeWhere((element) => element == squareg);
                list_square_grey_io
                    .sort((b, a) => b.count_gray.compareTo(a.count_gray));
                setState(() {});
                Navigator.of(context).pop();
//                setState(() {});
              },
            )
          ],
        );
      },
    );
  }

  String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }
}

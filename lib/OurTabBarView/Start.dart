import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Race.dart';
import 'package:manage_without/Models/TrackingWithInformation.dart';
import 'package:manage_without/ScreenManager/RaceInfo.dart';


import 'package:toast/toast.dart';

import '../Appearance.dart';
import '../Database.dart';
import 'package:http/http.dart' as http;

class Start extends StatefulWidget {
  Event_l event_l;
  Race race;

  Start(this.event_l, this.race);

  @override
  _StartState createState() => _StartState(event_l, race);
}

class _StartState extends State<Start> {
  Event_l event_l;
  ScrollController _scrollController;
  Race race;
  List<TrackingInformation> lst_tracking = new List();
  bool state = false;

  _StartState(this.event_l, this.race);

  @override
  void initState() {
    _scrollController = ScrollController();
    load();
    get_tracking();
    super.initState();
  }

  Future<void> load() {}

  get_tracking() async {
    await DBProvider.db.get_trackings_race(race, event_l,is_update: true);
  }

  @override
  Widget build(BuildContext context) {
    return Container(child: racesListView());
  }
  Color col_from_argb=Color(0xff121214);
  FutureBuilder<List<Race>> racesListView() {
    DBProvider.db.TrackingInformation_list().then((value) {
      lst_tracking = value;
    });

//    print("race ${item}");
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Race_list_from_event(event_l),
      builder: (BuildContext context, AsyncSnapshot<List<Race>> snapshot) {
//        print(event_l.toString());
        if (snapshot.hasData && snapshot.data.length > 0) {
//          print("before>>>> ${snapshot.data}");
          snapshot.data.sort((a, b) => a.startTimeMs.compareTo(b.startTimeMs));
//          print("after>>>> ${snapshot.data}");
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                Race item = snapshot.data[index];
                TrackingInformation trackingInformation =
                    lst_tracking.firstWhere(
                        (element) => element.reference == item.reference,
                        orElse: () => null);

                return Padding(
                  padding: EdgeInsets.only(
                      left: 3, right: 3, top: index == 0 ? 15 : 2),
                  child: Container(
                  decoration: BoxDecoration(
                      color: col_from_argb,
                      border: Border(
                          bottom: index != snapshot.data.length-1
                              ? BorderSide(
                              color: Colors.blueGrey[50],
                              width: 0.3)
                              : BorderSide(),
                          top: index == 0
                              ? BorderSide(
                              color: Colors.blueGrey[50],
                              width: 0.3)
                              : BorderSide())),
                    height: 90,
                    child: Stack(
                      children: <Widget>[
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 0.0, top: 2.0),
                                    child: Container(
                                      child: Row(
                                        children: <Widget>[
                                          Container(
//                                            width: 60,
                                            child: Padding(
                                              padding: const EdgeInsets.only(left: 8),
                                              child: InkWell(
                                                onTap: () {
                                                Navigator.push(
                                                    context,
                                                    new MaterialPageRoute(
                                                        builder: (context) =>
                                                            RaceInfo(event_l,
                                                                item)));
                                                },
                                                child: Text(
                                                  item.name,
                                                  style: TextStyle(
                                                      decoration: TextDecoration
                                                          .underline,
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                      FontWeight.w400),
                                                )
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 8),
                                            child: Text(
                                              "Start ${item.startTimeMs.toString().substring(10, 16)}",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: font_size_body,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          )
//
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                              padding: const EdgeInsets.only(right: 16.0,top: 4),
                              child: trackingInformation != null
                                  ? trackingInformation.status ==
                                          "waiting_for_gun"
                                      ? InkWell(
                                          onTap: () {
//                                  print(trackingInformation.status);
                                            start_race_from_server(item);
                                          },
                                          child: Container(
                                            child: Center(
                                              child: Text("Start",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w800)),
                                            ),
                                            width: 80,
                                            height: 40,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                gradient: LinearGradient(
                                                    colors: <Color>[
                                                      Colors.orange,
                                                      Colors.orange,
                                                      Colors.orange,
                                                      Colors.orange,
                                                      Colors.deepOrange,
                                                      Colors.deepOrange
                                                    ],
                                                    begin: Alignment.topLeft,
                                                    end:
                                                        Alignment.bottomRight)),
//                                        child: Center(
//                                          child: ,
//                                        ),
                                          ),
                                        )
                                      : trackingInformation.status == "finished"
                                          ? Container(
                                              child: Center(
                                                child: Text("Finished",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.w800)),
                                              ),
                                              width: 80,
                                              height: 40,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10)),
                                                  gradient: LinearGradient(
                                                      colors: <Color>[
                                                        Colors.red,
                                                        Colors.red,
                                                        Colors.red,
                                                        Colors.red,
                                                        Colors.redAccent,
                                                        Colors.redAccent
                                                      ],
                                                      begin: Alignment.topLeft,
                                                      end: Alignment
                                                          .bottomRight)),
//                                        child: Center(
//                                          child: ,
//                                        ),
                                            )
                                          : Text(
                                              "Race has started",
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w800),
                                            )
                                  : Text(
                                      "Loading.....",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w900),
                                    )),
                        ),
                      ],
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            color: Colors.blue,
            size: 17,
          ));
        } else if (snapshot.data.length == 0) {
          Center(
              child: Text(
            "No Races Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }
        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.green,
          ),
        );
      },
    );
  }

  Future<void> start_race_from_server(Race race) async {
//    print(race.toString());
    try {
      await http
          .post(
              'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/gun')
          .then((data) async {
//      races = [];
        Map<String, dynamic> dataServer = jsonDecode(data.body);

        if (dataServer["status"] == "guned") {
//          print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${dataServer}");

          TrackingInformation trackingInformation =
              new TrackingInformation.fromJSONSERVER(dataServer, race,event_l);

          await DBProvider.db.insert_tracking(trackingInformation);
          startRace(race, true);
//          print(race.toString());
          return;
        }
//        startRace(race,false);
      });
    } catch (ex) {
//      startRace(race,false);
    }
  }

  void startRace(Race race, bool state) {
    race.is_start = state;
    DBProvider.db.update_race(race);
    setState(() {});
  }
}

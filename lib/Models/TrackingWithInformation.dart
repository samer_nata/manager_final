import 'dart:convert';

import 'Event.dart';
import 'Race.dart';



class TrackingInformation {
  String reference, status,ref_event;

  DateTime startTimeMs, openingTimeMs, gunTimeMs;

  int id, duration;
  String race;
  int courseId;

//  Map<String, dynamic> jso = Map();
//  List<Timing> timing = new List();
//  List<Tracker> trackers = new List();

//Time startTimeMs;
  TrackingInformation({
    this.startTimeMs,
    this.reference,
    this.gunTimeMs,
    this.openingTimeMs,
    this.status,
    this.race,
    this.id,
    this.duration,this.courseId,this.ref_event
  });

  factory TrackingInformation.fromJSONSERVER(Map<String, dynamic> json,Race race,Event_l event_l) {

//    List list = json['timings'];
//    jsonDecode(json);
    if (json != null) {
      return TrackingInformation(
          status: json["status"],
          reference: json["reference"],
          gunTimeMs: json["gunTimeMs"] != null
              ? DateTime.fromMillisecondsSinceEpoch(json["gunTimeMs"])
              : null,
          openingTimeMs: json.containsKey("openingTimeMs")
              ? DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"])
              : null,
          startTimeMs: json.containsKey("startTimeMs")
              ? DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"])
              : null,
          duration: json.containsKey('duration') ? json['duration'] : null,
          id: json.containsKey('id') ? json['id'] : null,
          race: json.containsKey('race') ? json['race'] : null,courseId: race.courseId,ref_event: event_l.reference
      );
    }
    return null;
  }

//  factory TrackingInformation.fromJSON(Map<String, dynamic> json) {
////    List list = json['timings'];
////    jsonDecode(json);
//    try {
//      Map<String, dynamic> jr = jsonDecode(json['jso']);
//
//      if (json != null) {
//        return TrackingInformation(
//            status: json["status"],
//            reference: json["reference"],
//            gunTimeMs: json["gunTimeMs"] != null
//                ? DateTime.fromMicrosecondsSinceEpoch(json["gunTimeMs"])
//                : null,
//            openingTimeMs: json.containsKey("openingTimeMs")
//                ? DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"])
//                : null,
//            startTimeMs: json.containsKey("startTimeMs")
//                ? DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"])
//                : null,
//            timing: List.generate(jr['timings'].length, (i) {
//              return Timing.fromJSON(jr['timings'][i]);
//            }),
//            duration: json['duration'],
//            id: json['id'],
//            race: json['race'],
//            trackers: List.generate(jr['trackers'].length, (i) {
//              return Tracker.fromJSON(jr['trackers'][i]);
//            }));
//      }
//    } catch (ex) {
//      print(ex);
//    }
//    return null;
//  }

  factory TrackingInformation.fromJSON(Map<String, dynamic> json) {
    return TrackingInformation(
        status: json["status"],
        reference: json["reference"],
        gunTimeMs: json["gunTimeMs"] != null
            ? DateTime.fromMillisecondsSinceEpoch(json["gunTimeMs"])
            : null,
        openingTimeMs: json.containsKey("openingTimeMs")
            ? DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"])
            : null,
        startTimeMs: json.containsKey("startTimeMs")
            ? DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"])
            : null,
        duration: json['duration'],
        id: json['id'],
        race: json['race'],courseId: json['courseId'],ref_event: json['ref_event']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "status": status,
      "startTimeMs": startTimeMs.millisecondsSinceEpoch,
      "openingTimeMs": openingTimeMs.millisecondsSinceEpoch,
      "gunTimeMs": gunTimeMs != null ? gunTimeMs.millisecondsSinceEpoch : null,
      "reference": reference,
      "race": race,
      "duration": duration,"courseId":courseId,'ref_event':ref_event
    };
  }

  @override
  String toString() {
    return 'TrackingInformation{reference: $reference, status: $status, ref_event: $ref_event, startTimeMs: $startTimeMs, openingTimeMs: $openingTimeMs, gunTimeMs: $gunTimeMs, id: $id, duration: $duration, race: $race, courseId: $courseId}';
  }
}

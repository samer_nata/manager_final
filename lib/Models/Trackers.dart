class Tracker {
  int id, bib, duration;
  DateTime regTimeMs, checkinTimeMs, gunTimeMs;
  String status, type, participantRef;
  List<String> devices;
  double distance;
  Map<String, dynamic> position = Map();
  Map<String, dynamic> result = Map();

//Time startTimeMs;
  Tracker(
      {this.participantRef,
      this.bib,
      this.id,
      this.status,
      this.type,
      this.checkinTimeMs,
      this.devices,
      this.regTimeMs,
      this.position,
      this.duration,
      this.gunTimeMs,
      this.distance,this.result});

  factory Tracker.fromJSON(Map<String, dynamic> json) {
//    jsonDecode(json);

    if (json != null) {
      return Tracker(
          bib: json["bib"],
          id: json['id'],
          participantRef: json['participantRef'],
          type: json['type'],
          checkinTimeMs: json.containsKey('checkinTimeMs')
              ? DateTime.fromMillisecondsSinceEpoch(json['checkinTimeMs'])
              : null,
          devices: List.generate(
              json["devices"].length, (index) => json["devices"][index]),
          regTimeMs: json.containsKey('regTimeMs')
              ? DateTime.fromMillisecondsSinceEpoch(json['regTimeMs'])
              : null,
          status: json['status'],
          position: json['position'],
          distance: json['distance'],
          duration: json['duration'],
          gunTimeMs: json.containsKey('gunTimeMs')
              ? DateTime.fromMillisecondsSinceEpoch(json['gunTimeMs'])
              : null,result: json.containsKey('result')?json['result']:null);
    }
    return null;
  }

  @override
  String toString() {
    return 'Tracker{id: $id, bib: $bib, regTimeMs: $regTimeMs, checkinTimeMs: $checkinTimeMs, status: $status, type: $type, participantRef: $participantRef, devices: $devices}';
  }
}

import 'package:flutter/cupertino.dart';

class User {
  String firstName;
  String lastName;
  int bibNumber;
  String club;
  String division;

  User(
      {@required this.firstName,
      @required this.lastName,
      @required this.bibNumber,
      this.club,
      this.division});

}

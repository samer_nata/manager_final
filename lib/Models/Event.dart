

class Event_l {

  DateTime startTimeMs;
  String defaultRace;
  DateTime endTimeMs;
  String location;
  String name;

//  JSON races;

  String reference;

  Event_l(
      {this.startTimeMs,
      this.endTimeMs,
      this.defaultRace,
      this.location,
      this.name,
//      this.races,
      this.reference,
      });

  factory Event_l.fromJSON(json) {
    return Event_l(

      startTimeMs: DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"]),
      name: json["name"],
      defaultRace: json["defaultRace"],
      location: json["location"],
      endTimeMs: DateTime.fromMillisecondsSinceEpoch(json["endTimeMs"]),
      reference: json['reference'],
//        races: json['races']
    );
  }

  factory Event_l.fromJSONServer(json) {
    return Event_l(
      startTimeMs: DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"]),
      name: json["name"],
      defaultRace: json["defaultRace"],
      location: json["location"],
      endTimeMs: DateTime.fromMillisecondsSinceEpoch(json["endTimeMs"]),
      reference: json['reference'],
//        races: json['races']
    );
  }

  Map<String, dynamic> toMap() => {
        "startTimeMs": startTimeMs.millisecondsSinceEpoch,
        "name": name,
        "endTimeMs": endTimeMs.millisecondsSinceEpoch,
        "defaultRace": defaultRace,
        "location": location,
//        "races": races,
        'reference': reference
      };

  @override
  String toString() {
    return 'Event{, startTimeMs: $startTimeMs, defaultRace: $defaultRace, endTimeMs: $endTimeMs, location: $location, name: $name, reference: $reference}';
  }
}

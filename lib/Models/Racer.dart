class Racer {
  int id, bib;
  String club;
  String surname, firstname, ref_event, ref_race;

//  int ref_race;

  Racer(
      {this.id,
        this.bib,
        this.firstname,
        this.club,
        this.surname,
        this.ref_event,
        this.ref_race
//      this.ref_race,
      });

  factory Racer.fromJSONserver(json) {
    return Racer(
      id: int.parse(json["id"]),
      club: json["club"],
      surname: json["surname"],
      firstname: json["firstname"],
      bib: json["bib"],
    );
  }

  factory Racer.fromJSON(Map<String, dynamic> json) {
    return Racer(
      id: json["id"],
      bib: json["bib"],
      club: json["club"],
      ref_event: json['ref_event'],
      ref_race: json['ref_race'],

      surname: json["surname"],

      firstname: json["firstname"],
//        ref_race: json['ref_race']
    );
  }

  Map<String, dynamic> to_Map() => {
    'id': id,
    'ref_event': ref_event,
    'club': club,

    'surname': surname,
    'bib': bib,
    "firstname": firstname, 'ref_race': ref_race
//        "ref_race": ref_race,
  };

  @override
  String toString() {
    return 'Racer{id: $id, bib: $bib, club: $club, surname: $surname, firstname: $firstname, ref_event: $ref_event, ref_race: $ref_race}';
  }
}

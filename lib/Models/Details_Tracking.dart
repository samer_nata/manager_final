import 'dart:convert';



import 'Race.dart';
import 'Timings.dart';
import 'Trackers.dart';
import 'TrackingWithInformation.dart';

class DetailsTracking {
  int id, id_p;
  Map<String,dynamic> timings, trackers;
  List<Timing> lst_timings = new List();
  List<Tracker> lst_trackers = new List();
  String reference_race;

//Time startTimeMs;
  DetailsTracking(
      {this.id,
      this.trackers,
      this.timings,
      this.lst_timings,
      this.lst_trackers,
      this.reference_race,
      this.id_p});

  factory DetailsTracking.fromJSONSERVER(Map<String, dynamic> json,
      TrackingInformation trackingInformation, Race race) {
//    List list = json['timings'];
//    jsonDecode(json);
    if (json != null) {
      return DetailsTracking(
          id: trackingInformation.id,
          id_p: trackingInformation.id,
          timings: {'timings': json['timings']},
          trackers: {'trackers': json['trackers']},
          reference_race: race.reference);
    }
    return null;
  }

  factory DetailsTracking.fromJSON(Map<String, dynamic> json) {
    Map<String, dynamic> jr_tim = jsonDecode(json['timings']);
    List jr_ti = jr_tim['timings'];
    Map<String, dynamic> jr_trackers = jsonDecode(json['trackers']);
    List jr_tr = jr_trackers['trackers'];
//    print(">>>>>> ${json}");
    return DetailsTracking(
        id: json['id'],
        timings: jsonDecode(json['timings']),
        trackers: jsonDecode(json['trackers']),
        reference_race: json['reference_race'],
        id_p: json['id_p'],
        lst_timings: List.generate(jr_ti.length, (i) {
          return Timing.fromJSON(jr_ti[i]);
        }, growable: true),
        lst_trackers: List.generate(jr_tr.length, (i) {
          return Tracker.fromJSON(jr_tr[i]);
        }, growable: true));
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "timings": jsonEncode(timings),
      "trackers": jsonEncode(trackers),
      'id_p': id_p,
      "reference_race": reference_race
    };
  }

  @override
  String toString() {
    return 'DetailsTracking{id: $id, timings: $timings, trackers: $trackers, lst_timings: $lst_timings, lst_trackers: $lst_trackers, reference_race: $reference_race}';
  }
}

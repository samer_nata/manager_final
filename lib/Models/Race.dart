import 'dart:collection';
import 'dart:convert';

import 'package:manage_without/Models/Racer.dart';

import 'Tracking.dart';

class Race {
  int id;
  DateTime startTimeMs;
  String name;

  int courseId;

  String reference;
  bool is_start = false;
  String ref_event;
  Map<String,dynamic> racers;
  List<Racer> list_racers = new List();

  dynamic jso;

//Time startTimeMs;
  Race(
      {this.id,
      this.name,
      this.startTimeMs,
      this.courseId,
      this.reference,
      this.is_start,
      this.ref_event,
      this.jso,
      this.racers,
      this.list_racers});

  factory Race.fromJSON(Map<String, dynamic> json) {

    List list_ra=jsonDecode(json['racers'])['startlist'];
//    List list_ra=new List();
//    list_ra=jsonDecode(json['racers'])['startlist'];

    return Race(
        id: json["id"],
        ref_event: json["ref_event"],
        name: json["name"],
        reference: json["reference"],
        courseId: json['courseId'],
        startTimeMs: DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"]),
        is_start: json["is_start"] == 0 ? false : true,
        jso: jsonDecode(json['jso']),
        racers: jsonDecode(json['racers']),
        list_racers: List.generate(list_ra.length, (i) {
      return Racer.fromJSONserver(list_ra[i]);
    }, growable: true)


//        js_tracking: json["tracking"],

        );
  }

  factory Race.fromJSONSERVER(Map<String, dynamic> json) {
    bool d = true;
    return Race(
      id: json["id"],
      ref_event: json["ref_event"],
      name: json["name"],
//          tracking: json["tracking"] != null ? Tracking.fromJSON(json["tracking"]) : null,
      reference: json["reference"],
      courseId: json['courseId'],
      is_start: d ? true : false,
      startTimeMs: DateTime.fromMillisecondsSinceEpoch(json["startTimeMs"]),
      jso: {"splitNames": json["splitNames"], "categories": json["categories"]},

//          js_tracking: json["tracking"],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "ref_event": ref_event,
      "id": id,
      "name": name,
      "startTimeMs": startTimeMs.millisecondsSinceEpoch,
      "courseId": courseId,
      "is_start": is_start == false ? 0 : 1,
      "jso": jsonEncode(jso),
      "reference": reference,
      "racers": jsonEncode(racers)
    };
  }

  @override
  String toString() {
    return 'Race{id: $id, name: $name, racers: $racers, list_racers: $list_racers}';
  }
}

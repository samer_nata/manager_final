import 'dart:convert';
import 'package:intl/intl.dart' as nl;

class Timing implements Comparable {
  String courspoint, category, position, positionCat, source, participantRef;
  int bib, duration;
  String time;
  int timer = 0;
  int temp_time;
  bool is_start=false;

//Time startTimeMs;
  Timing(
      {this.duration,
      this.bib,
      this.category,
      this.position,
      this.courspoint,
      this.participantRef,
      this.positionCat,
      this.source,
      this.time,
      this.timer,this.temp_time,this.is_start});

  factory Timing.fromJSON(Map<String, dynamic> json) {
//    print("HIIIII ${json["time"]}");
//    jsonDecode(json);
    if (json != null) {
//      print(new DateFormat.yMMMd().format(new DateTime.now()));
//   final DateFormat alfrescoDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy")
//

      return Timing(
          bib: json["bib"],
          time: json["time"],
          duration: json["duration"],
          courspoint: json['courspoint'],
          category: json['category'],
          source: json['source'],
          participantRef: json['participantRef'],
          position: json['position'],
          positionCat: json['positionCat'],temp_time: json.containsKey("temp")?json['temp']['timeMs']:null,
          timer: json.containsKey("timer") ? json['timer'] : 0,is_start:json.containsKey("is_start")?json['is_start']:false );
    }
    return null;
  }


  @override
  String toString() {
    return 'Timing{participantRef: $participantRef, bib: $bib, time: $time, timer: $timer}';
  }

  @override
  int compareTo(other) {
    // TODO: implement compareTo
    if (bib > other.txt_bib_controller) {
      return bib;
    }
    return other.txt_bib_controller;
  }
}

import 'dart:collection';
import 'dart:convert';


import 'Coursepoints.dart';



class Course {
  int id;
  double distance;
  Map<String, dynamic> startPos;
  Map<String, dynamic> endPos;
  List<Coursepoints> coursepoints=new List();
  String name;
  dynamic js_coursepoints;

//Time startTimeMs;
  Course(
      {this.id,
      this.name,
      this.distance,
      this.js_coursepoints,
      this.startPos,
      this.endPos,
      this.coursepoints,
       });

  factory Course.fromJSON(Map<String, dynamic> json) {
    return Course(
        id: json["id"],
        name: json["name"],
        distance: json['distance'],
        js_coursepoints:jsonDecode(json['js_coursepoints']),
        endPos: jsonDecode(json['endPos']),
        startPos: jsonDecode(json['startPos']),
        coursepoints: json['js_coursepoints'] != null
            ? Coursepoints.get_coursetpoint(jsonDecode(json['js_coursepoints']))
            : null,
    );
  }

  factory Course.fromJSONServer(Map<String, dynamic> json) {
    return Course(
        id: json["id"],
        name: json["name"],
        distance: double.parse(json['distance']),
        js_coursepoints: json['coursepoints'],
        endPos: json['endPos'],
        startPos: json['startPos'],
        coursepoints: json['coursepoints'] != null
            ? Coursepoints.get_coursetpoint(json['coursepoints'])
            : null
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "name": name,
      "distance": distance,
      "js_coursepoints": jsonEncode(js_coursepoints),
      "endPos": jsonEncode(endPos),
      "startPos": jsonEncode(startPos),


    };
  }

  @override
  String toString() {
    return 'Course{id: $id, distance: $distance, startPos: $startPos, endPos: $endPos, coursepoints: $coursepoints, name: $name, js_coursepoints: $js_coursepoints}';
  }
}

import 'dart:async';

import 'Models/Course.dart';
import 'Models/Details_Tracking.dart';
import 'Models/Timings.dart';




class Square {
  Function function;
  String txt;

  int bib;
  double km;

int count_gray=0;
  DetailsTracking detailsTracking;
  bool is_start_from_before = false;
  int time_in_mill;

//  Timer timer_square=new Timer(const Duration(seconds: 1), (){});

  bool is_start = false;
  bool is_start_from_page = false;
  int timerMaxSeconds;
  Course course;
  bool is_finish = false;
  List<Timing> lst_timings = new List();
  int currentSeconds = 0;
  bool is_stop = false;
  bool is_hide=false;
  Map<String,dynamic>map_gray=new Map();
  bool start_animation = false;
  bool is_delete=false;

  Square(
      {this.txt,
      this.timerMaxSeconds,
      this.detailsTracking,
      this.course,
      this.lst_timings,
      this.bib,
      this.is_start_from_page,
      this.time_in_mill,this.count_gray, this.function});

  Duration interval = Duration(seconds: 1);
  Duration increase = Duration(seconds: 0);
//bool is_delete=false;
//  final int timerMaxSeconds = 10;

  Future<void> startTimeout() {
    print("req>>>>>>>>>>>>>>>>>>>>");


    this.is_start_from_before = true;
//   this. function.call();

//    var duration = this.interval;
//    if (this.currentSeconds != 0 && !this.is_finish) {
//      this.currentSeconds = this.timerMaxSeconds - this.increase.inSeconds;
//    }

    if(this.is_start_from_page){
      Timer.periodic(this.interval, (timer) async {

        print("Cure>>>>>>>>>>>>>>>> ${this.currentSeconds} bib ${this.bib}");
        print("start_anima>>>>>>>>>>>>>>>> ${this.start_animation}");
        print("${this.bib} this.increase.inSeconds>>>>>>>>>>>>>> ${this.increase.inSeconds}");
        if(this.increase.inSeconds>=this.timerMaxSeconds){

          this.is_finish = true;
          this.start_animation=false;

          cancel();
          timer.cancel();
          if(this.function!=null){
            this. function.call();
          }
return;
        }
        if (is_stop) {
          this.currentSeconds = this.timerMaxSeconds - this.increase.inSeconds;
//        interval = Duration(seconds: 1);
//          this. function.call();
          timer.cancel();
          return;
        }
        if (this.is_start == true) {
          this.currentSeconds = this.timerMaxSeconds - timer.tick;

//          function.call();
          if (this.increase.inSeconds>= this.timerMaxSeconds) {
            timer.cancel();

//          this.currentSeconds=0;
//        this.is_start_from_page=false;
            this.is_finish = true;

            this.start_animation=false;
            if(this.function!=null){
              this. function.call();
            }
            return;
          }



          this.currentSeconds = this.timerMaxSeconds - this.increase.inSeconds;
          this.increase = Duration(seconds: this.increase.inSeconds + 1);
          if( this.currentSeconds==3){
            print('currranima>>>>> ${this.currentSeconds}');
            this.start_animation=true;

        return;
          }

        }
        if(this.function!=null){
          this. function.call();
        }

      });
    }


  }


  @override
  String toString() {
    return 'Square{txt: $txt, bib: $bib, count_gray: $count_gray, time_in_mill: $time_in_mill, is_hide: $is_hide, map_gray: $map_gray}';
  }

  Future<void> cancel() {
    this.is_start = false;
    this.is_stop = true;
//    this. function.call();
//    this. function.call();

//
//    this.is_finish=false;
//    this.is_start_from_page=false;
//   if(this.timer_square.isActive){
//     this.timer_square.cancel();
//   }
  }

  Future<void> start() {
    this.is_start = true;
    this.is_stop = false;

//function.call();
    if(this.is_start_from_page){
      this.startTimeout();
    }
  }
}

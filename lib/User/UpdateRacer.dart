import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:manage_without/Models/Race.dart';
import 'package:manage_without/Models/Racer.dart';

import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:manage_without/Models/Event.dart';

import '../Database.dart';

class UpdateRacers extends StatefulWidget {
  Race race;
  Event_l event_l;

  UpdateRacers(this.race, this.event_l);

  @override
  _UpdateRacersState createState() => _UpdateRacersState(race, event_l);
}

class _UpdateRacersState extends State<UpdateRacers> {
  ScrollController _scrollController;

  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  Race race;
  Event_l event_l;

  _UpdateRacersState(this.race, this.event_l);

  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController clubController = new TextEditingController();

  String firstName, lastName, club;

  @override
  void initState() {
    _scrollController = ScrollController();

    super.initState();
  }

  Widget updateSheet(Racer racer) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                color: Color(0xff121214),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16.0, bottom: 8.0),
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        firstName = value;
                      });
                    },
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w100),
                    textAlign: TextAlign.left,
                    controller: firstNameController,
                    decoration: new InputDecoration(
                      contentPadding: EdgeInsets.only(
                          left: 8, right: 8.0, bottom: 8.0, top: 10),
                      //border: InputBorder.none,
                      hintStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w100),
                      hintText: 'First name', //'Ditt förnamn'
                    ),
                  ),
                ),
              ),
              Container(
                color: Color(0xff121214),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16.0, bottom: 8.0),
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        lastName = value;
                      });
                    },
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w100),
                    textAlign: TextAlign.left,
                    controller: lastNameController,
                    decoration: new InputDecoration(
                      contentPadding: EdgeInsets.only(
                          left: 8, right: 8.0, bottom: 8.0, top: 10),
                      //border: InputBorder.none,
                      hintStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w100),
                      hintText: 'Last name', //'Ditt efternamn'
                    ),
                  ),
                ),
              ),
              Container(
                color: Color(0xff121214),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16.0, bottom: 8.0),
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        club = value;
                      });
                    },
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w100),
                    textAlign: TextAlign.left,
                    controller: clubController,
                    decoration: new InputDecoration(
                      contentPadding: EdgeInsets.only(
                          left: 8, right: 8.0, bottom: 8.0, top: 10),
                      //border: InputBorder.none,
                      hintStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w100),
                      hintText: 'Club', //'Din klubb (valfri)'
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(top: 24.0),
                  child: InkWell(
                    onTap: () async {
                   await   doUpdate(racer).then((value) async {

                      });
                    }
                    ,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      width: 200,
                      height: 50,
                      child: Center(
                        child: Text(
                          "Confirm",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, left: 8.0),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Tillbaka",
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 16.0, top: 16.0),
          child: Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: Color(0xff121214),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget listracers() {
    return ListView.builder(
        itemCount: race.list_racers.length,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) {
          Racer item = race.list_racers[index];

          return Padding(
            padding: const EdgeInsets.all(1.0),
            child: Container(
              color: Color(0xff121214),
              height: 90,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Container(
                            width: 275,
                            child: Text(
                              item.firstname + " " + item.surname,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w100),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Start ${item.club}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w100),
                                ),
//                                      Text(
//                                        "  ${item.gender} ",
//                                        style: TextStyle(
//                                            color: Colors.white,
//                                            fontSize: 15,
//                                            fontWeight: FontWeight.w100),
//                                      ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            firstNameController.text = item.firstname;
                            lastNameController.text = item.surname;
                            clubController.text = item.club;
                          });
                          _scaffoldKey.currentState.showBottomSheet(
                              (context) => updateSheet(
                                    item,
                                  ),
                              backgroundColor: Colors.black);
                        },
                        child: Icon(
                          Icons.edit,
                          color: Colors.white70,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

//  FutureBuilder<List<Racer>> listAllRaces() {
//    return FutureBuilder(
//      initialData: [],
//      future: DBProvider.db.Racer_list_from_race(race),
//      builder: (BuildContext context, AsyncSnapshot<List<Racer>> snapshot) {
//        if (snapshot.hasData && snapshot.data.length > 0) {
//
//        } else if (snapshot.data.length == 0 || snapshot == null) {
//          return Center(
//              child: Text(
//                "No Racers Here",
//                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
//              ));
//        }
//
//        return Center(
//          child: CircularProgressIndicator(
//            backgroundColor: Colors.green,
//          ),
//        );
//      },
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: PreferredSize(
        child: Container(
          height: 175,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Stack(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil(
                                              '/RacesScreen',
                                              (Route<dynamic> route) => false);
                                    },
                                    child: Container(
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/LaLigaLogo.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          //color: Colors.white,
                                          width: 200,
                                          child: Center(
                                            child: Text(
                                              race.name,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w100),
                                            ),
                                          ),
                                        ),
//                                        Padding(
//                                          padding:
//                                          const EdgeInsets.only(top: 16.0),
//                                          child: Container(
//                                            width: 200,
//                                            child: Center(
//                                              child: Text(
//                                                "*",
//                                                style: TextStyle(
//                                                    color: Colors.white54,
//                                                    fontSize: 15,
//                                                    fontWeight:
//                                                    FontWeight.w100),
//                                              ),
//                                            ),
//                                          ),
//                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.blue,
                              height: 2,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      ),
      body: listracers(),
    );
  }

  Future doUpdate(Racer racer) async {
    setState(() {
      firstName = firstNameController.text.trim();
      lastName = lastNameController.text.trim();
      club = clubController.text.trim();
    });
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (firstName.isEmpty && lastName.isEmpty && club.isEmpty) {
          Toast.show(
              "Vänligen skriv ditt förnamn , ditt efternamn , ditt startnummer , club , gender , category",
              context,
              backgroundColor: Colors.black,
              duration: Toast.LENGTH_LONG);
        } else if (firstName.isEmpty || lastName.isEmpty || club.isEmpty) {
          if (firstName.isEmpty) {
            Toast.show("Vänligen skriv ditt förnamn", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          } else if (lastName.isEmpty) {
            Toast.show("Vänligen skriv ditt efternamn", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          } else if (club.isEmpty) {
            Toast.show("Vänligen skriv ditt club", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          }
        } else {
          var bodys = jsonEncode({
            "id": racer.id.toString(),
            "bib": racer.bib,
            "firstname": firstName,
            "surname": lastName,
            "club": club,
          });
          http
              .put(
                  'http://54.77.120.67:8080/rest/_App/races/${race.reference}/startlist/participants/${racer.bib}?format=LaLiga',
                  headers: <String, String>{
                    'Content-Type': 'application/json;charset=UTF-8',
                  },
                  body: bodys)
              .then((val) async {
            if (val.body.isEmpty) {
              print("Empty");
            } else {
              Map<String, dynamic> data = jsonDecode(val.body);
//              print(">>>>>>>>>>>>>>>>>>>>>> $data");
//              Racer racern=new Racer.fromJSONserver(data);
//              racern.id=racer.id;Fc
              List list_old = race.racers['startlist'];
              print('data>>>>> ${data}');
              print('list_old>>> ${list_old}');
              list_old
                  .forEach((element) {
                    if(element['bib'].toString()==data['bib'].toString()){
                    list_old[  list_old.indexOf(element)]=data;

                    }
              });

              await race.racers.update('startlist', (value) => list_old);
              print('after>>>> ${race.racers}');
              await DBProvider.db.insert_race(race).then((value) async {
                await  DBProvider.db.Race_list_refresh(race, event_l).then((value){
                  if(value.length>0){
                    setState(() {
                      race=value[0];
                    });
                  }
                });
                Toast.show("Operationen lyckades", context,
                    backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
              });

            }


          });
        }
      }
    } on SocketException catch (_) {
      Toast.show("Check internet connection !!", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    }

    Navigator.pop(context);
  }
}

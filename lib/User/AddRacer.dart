import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:manage_without/Models/Manager.dart';
import 'package:manage_without/Models/Race.dart';
import 'package:manage_without/Models/Racer.dart';

import 'package:toast/toast.dart';

import '../Database.dart';
import 'package:http/http.dart' as http;

class AddRacer extends StatefulWidget {
  Race race;

  AddRacer(this.race);

  @override
  _AddRacerState createState() => _AddRacerState(race);
}

class _AddRacerState extends State<AddRacer> {
  List<Manager> manager_list = new List();
  Race race;

  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController clubController = new TextEditingController();

//  TextEditingController divisionController = new TextEditingController();
//  TextEditingController genderController = new TextEditingController();
//  TextEditingController categoryController = new TextEditingController();
  TextEditingController bibController = new TextEditingController();

  String firstName, lastName, club, bibNumber;

  _AddRacerState(this.race);

  @override
  void initState() {
    DBProvider.db.Manager_list().then((value) {
      setState(() {
        manager_list = value;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: PreferredSize(
        child: Container(
          height: 175,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Stack(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil(
                                              '/RacesScreen',
                                              (Route<dynamic> route) => false);
                                    },
                                    child: Container(
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/LaLigaLogo.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          //color: Colors.white,
                                          width: 200,
                                          child: Center(
                                            child: Text(
                                              //
                                              race.name,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w100),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.blue,
                              height: 2,
                            ),
                          )
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 12.0, top: 0.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 16.0),
                                child: InkWell(
                                  onTap: () {
                                    addRacer();
                                  },
                                  child: Container(
                                    width: 20,
                                    height: 25,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: manager_list.length == 0
                                                ? AssetImage(
                                                    'Images/profile.png')
                                                : AssetImage(
                                                    'Images/greenProfile.png'),
                                            fit: BoxFit.fill)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
              color: Color(0xff121214),
              width: MediaQuery.of(context).size.width,
              height: 450,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Color(0xff121214),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8.0),
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              firstName = value;
                            });
                          },
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                          textAlign: TextAlign.left,
                          controller: firstNameController,
                          decoration: new InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 8, right: 8.0, bottom: 8.0, top: 10),
                            //border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w100),
                            hintText: 'First name', //'Ditt förnamn'
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Color(0xff121214),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8.0),
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              lastName = value;
                            });
                          },
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                          textAlign: TextAlign.left,
                          controller: lastNameController,
                          decoration: new InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 8, right: 8.0, bottom: 8.0, top: 10),
                            //border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w100),
                            hintText: 'Last name', //'Ditt efternamn'
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Color(0xff121214),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8.0),
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              club = value;
                            });
                          },
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                          textAlign: TextAlign.left,
                          controller: clubController,
                          decoration: new InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 8, right: 8.0, bottom: 8.0, top: 10),
                            //border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w100),
                            hintText: 'Club', //'Din klubb (valfri)'
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Color(0xff121214),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8.0),
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              bibNumber = value;
                            });
                          },
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                          textAlign: TextAlign.left,
                          controller: bibController,
                          decoration: new InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 8, right: 8.0, bottom: 8.0, top: 10),
                            //border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w100),
                            hintText: 'Bib', //'Din klubb (valfri)'
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 24.0),
                      child: InkWell(
                        onTap: () {
                          addRacer();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          width: 250,
                          height: 50,
                          child: Center(
                            child: Text(
                              "CONFIRM",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, bottom: 40),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "Tillbaka",
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 15,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future addRacer() async {
    setState(() {
      firstName = firstNameController.text.trim();
      lastName = lastNameController.text.trim();
      bibNumber = bibController.text.trim();
      club = clubController.text.trim();
    });

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (firstName.isEmpty &&
            lastName.isEmpty &&
            bibNumber.isEmpty &&
            club.isEmpty) {
          Toast.show(
              "Vänligen skriv ditt förnamn , ditt efternamn , ditt startnummer , club ,  ",
              context,
              backgroundColor: Colors.black,
              duration: Toast.LENGTH_LONG);
        } else if (firstName.isEmpty ||
            lastName.isEmpty ||
            bibNumber.isEmpty ||
            club.isEmpty) {
          if (firstName.isEmpty) {
            Toast.show("Vänligen skriv ditt förnamn", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          } else if (lastName.isEmpty) {
            Toast.show("Vänligen skriv ditt efternamn", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          } else if (bibNumber.isEmpty) {
            Toast.show("Vänligen skriv ditt startnummer", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          } else if (club.isEmpty) {
            Toast.show("Vänligen skriv ditt club", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          }
        }

        {
          var bodys = jsonEncode({
            "bib": int.parse(bibNumber),
            "firstname": firstName,
            "surname": lastName,
            "club": club,
          });
          http
              .post(
                  'http://54.77.120.67:8080/rest/_App/races/${race.reference}/startlist/participants?format=LaLiga',
                  headers: <String, String>{
                    'Content-Type': 'application/json;charset=UTF-8',
                  },
                  body: bodys)
              .then((val) async {
            if (val.body.isEmpty) {
              print("Empty");
            } else {
              var data = jsonDecode(val.body);
//              Racer racer = Racer.fromJSONserver(data);
              print('data>>>>>>>>>>>> ${data}');
              List list_old = race.racers['startlist'];
              print('list_old>>>');
              list_old.add(data);
              await race.racers.update('startlist', (value) => list_old);
              await DBProvider.db.insert_race(race);
            }
            Navigator.pop(context);
          });
        }
      }
    } on SocketException catch (_) {
      Toast.show("Check internet connection !!", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    }
  }
}

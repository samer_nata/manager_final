import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manage_without/Models/Event.dart';
import 'package:manage_without/Models/Manager.dart';
import 'package:manage_without/Models/Race.dart';
import 'package:manage_without/ScreenManager/EventsScreenManager.dart';
import 'package:manage_without/ScreenManager/RacesScreenManager.dart';


import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import '../Appearance.dart';
import '../ScreenManager/ControlPage.dart';
import '../Database.dart';

class LoginManager extends StatefulWidget {
//  Race race;
  Event_l _event_l;
  Race race;

  LoginManager(this._event_l, this.race);

  @override
  _LoginManagerState createState() => _LoginManagerState(_event_l, race);
}

class _LoginManagerState extends State<LoginManager> {
  Race race;
  Event_l _event_l;

  _LoginManagerState(this._event_l, this.race);

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  String email, password;
  bool isLoadingLogin = false;

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  Color col_from_argb = Color(0xff121214);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 30, left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "${_event_l.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),

                  ],
                ),
              ),
            ),

            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => EventsScreenManager(true)),
                          (route) => false);
                },
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Padding(
          padding: const EdgeInsets.only(top: 15, left: 3, right: 3),
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  decoration: BoxDecoration(
                      color: col_from_argb,
                      border: Border(
                          top: BorderSide(
                              color: Colors.blueGrey[50], width: 0.3))),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8.0),
                        child: Container(
                          child: TextField(
                            onChanged: (value) {
                              setState(() {
                                email = value;
                              });
                            },
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.left,
                            controller: emailController,
                            decoration: new InputDecoration(
                              contentPadding: EdgeInsets.only(
                                  left: 8, right: 8.0, bottom: 8.0, top: 10),
                              //border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.w400),
                              hintText: 'Username',
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8.0),
                        child: TextField(
                          obscureText: true,
                          onChanged: (value) {
                            setState(() {
                              password = value;
                            });
                          },
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.left,
                          controller: passwordController,
                          decoration: new InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 8, right: 8.0, bottom: 8.0, top: 10),
                            //border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400),
                            hintText: 'Password',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 150),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: InkWell(
                    onTap: () => login(),
                    child: isLoadingLogin
                        ? SpinKitThreeBounce(
                            size: 17,
                            color: Colors.blue,
                          )
                        : Container(
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            width: MediaQuery.of(context).size.width / 1.1,
                            height: 50,
                            child: Center(
                              child: Text(
                                "OK",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: font_size_body,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                  ),
                ),
              )
            ],
          )),
    );
  }

  Future login() async {
    String email = emailController.text.trim();
    String password = passwordController.text.trim();
//    Navigator.push(context,
//        new MaterialPageRoute(builder: (context) => new ControlPage(race)));

    if (email.isEmpty && password.isEmpty) {
      Toast.show("Vänligen skriv ditt förnamn , ditt efternamn ", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    } else if (email.isEmpty || password.isEmpty) {
      if (email.isEmpty) {
        Toast.show("Vänligen skriv ditt förnamn", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
      } else if (password.isEmpty) {
        Toast.show("Vänligen skriv ditt efternamn", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
      }
    } else if (!validateEmail(emailController.text.trim())) {
      Toast.show("Enter validate Email", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    } else {
      setState(() {
        isLoadingLogin = true;
      });
      try {
        await http.post('/login_manager.php', body: {
          'User_name': email,
          'User_password': password,
        }).then((data) async {
          setState(() {
            isLoadingLogin = false;
          });

          List list = jsonDecode(data.body);

if(1<2){}
 else {
//            print(list);
            Manager manager = new Manager.fromJSONserver(list[0]);
//            print(manager.toString());
//print(" login>>>>>>>>>>>>>>>>>>>>>>>> ${race.toString()}  ev   ${_event_l.toString()}");
            await DBProvider.db.insert_manager(manager).then((value) {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (_) => EventsScreenManager(false)),
                  (route) => false);
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => RacesScreenManager(_event_l)));
              if (race != null) {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => ControlPage(_event_l, race)));
              }
            });
          }
        });
      } catch (ex) {
        Toast.show("Try Again !", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
        setState(() {
          isLoadingLogin = false;
        });
        print(ex);
      }
    }
  }
}
